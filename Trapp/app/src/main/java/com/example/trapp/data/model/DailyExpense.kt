package com.example.trapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "daily_expense",
    foreignKeys = arrayOf(
        ForeignKey(entity = Budget::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("budget_id"),
        onDelete = ForeignKey.CASCADE)
    ))
data class DailyExpense(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    @ColumnInfo(name = "amount")
    val amount: Long? = null,
    @ColumnInfo(name = "day_number")
    val dayNumber: Long? = null,
    @ColumnInfo(name = "plan_id")
    val planId: String? = null,
    @ColumnInfo(name="budget_id")
    val budgetId: Long? = null
)

