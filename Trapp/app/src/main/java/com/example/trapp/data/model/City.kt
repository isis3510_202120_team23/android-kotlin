package com.example.trapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class City(
    @PrimaryKey val id: String,
    @ColumnInfo(name="latitude") val latitude: Double,
    @ColumnInfo(name="longitude") val longitude:Double,
    @ColumnInfo(name="name") val name:String,
    // timestamp = time of last update
    @ColumnInfo(name="timestamp" )
    var timestamp:Long? = null
    )