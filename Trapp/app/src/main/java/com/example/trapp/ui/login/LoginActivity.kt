package com.example.trapp.ui.login

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.trapp.R
import com.example.trapp.ui.plan.PlanActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import java.util.*


class LoginActivity : AppCompatActivity() {

    private val db = Firebase.firestore
    private var timeLoadingInit=0L
    private var timeLoadingEnd=0L
    private val timeSpent = 0

    private lateinit var initialTime: Calendar

    private lateinit var auth: FirebaseAuth
    val trace = Firebase.performance.newTrace("Loading time login Activity")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trace.start()
        timeLoadingInit= Calendar.getInstance().timeInMillis
        setContentView(R.layout.activity_login)
        // Initialize Firebase Auth
        auth = Firebase.auth
        val requestPermissionLauncherContact =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {

                } else {

                }
            }
        val permissionCheckContact = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS )

        if (permissionCheckContact== PackageManager.PERMISSION_DENIED)
        {
            requestPermissionLauncherContact.launch(
                Manifest.permission.READ_CONTACTS)
        }
        val requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {

                } else {

                }
            }
        val permissionCheck =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            requestPermissionLauncher.launch(
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
        setup()
    }

    override fun onStart() {
        super.onStart()
        trace.stop()
        initialTime = Calendar.getInstance()
        timeLoadingEnd= Calendar.getInstance().timeInMillis
        writeLoadingTime("SignIn", timeLoadingEnd-timeLoadingInit)
    }
    private fun writeLoadingTime(activity: String, time: Long) {

        val userTime = hashMapOf(
            "activity" to activity,
            "time" to time
        )
        db.collection("loadingTime").add(userTime)
    }

    override fun onStop() {
        super.onStop()
        val finalTime = Calendar.getInstance()
        val millis1: Long = initialTime.timeInMillis
        val millis2: Long = finalTime.timeInMillis

        // Calculate difference in milliseconds
        val diff = millis2 - millis1

        // Calculate difference in seconds
        val timeSpent = diff / 1000

        //save to database...

        writeNewTime("SignIn", timeSpent)
    }

    fun writeNewTime(activity: String, time: Long) {

        val userTime = hashMapOf(
            "activity" to activity,
            "time" to time
        )
        db.collection("userTime").add(userTime)
    }

    private fun setup() {

        val button = findViewById<Button>(R.id.signInButton)


        button.setOnClickListener{
            val email = findViewById<EditText>(R.id.editText_email_signin).text.toString()
            val password = findViewById<EditText>(R.id.editText_password_signin).text.toString()

            if(email.isNotEmpty() && password.isNotEmpty())
            {
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Falta pasarle el user a plan pa que haga algo
                            val planIntent = Intent(this, PlanActivity::class.java)
                            startActivity(planIntent)
                        } else {
                            Snackbar.make( findViewById(R.id.myCoordinatorLayout), "Login failed:" + task.exception.toString().split(":")[1], Snackbar.LENGTH_SHORT).show()

                            // If sign in fails, display a message to the user.
                            Log.w(ContentValues.TAG, "createUserWithEmail:failure", task.exception)
                        }
                    }

            }
        }



    }
}