package com.example.trapp.data.model

data class Review(
    //val id: String,
    // en firebase aparece como 'califiction'
    val calification: Double,
    val rationale:String
)