package com.example.trapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Trip(
    @PrimaryKey val id:String,
    @ColumnInfo(name = "img")val img: String,
    @ColumnInfo(name = "name")val name: String,
    @ColumnInfo(name = "price")val price: Long,
    @ColumnInfo(name = "startDate")val startDate: String,
    @ColumnInfo(name = "endDate")val endDate: String,
    @ColumnInfo(name = "save_date")
    var saveDate: Long?=null
    )