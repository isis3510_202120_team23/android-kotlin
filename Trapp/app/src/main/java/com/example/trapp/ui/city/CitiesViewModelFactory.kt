package com.example.trapp.ui.city

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.city.CityRepository

class CitiesViewModelFactory(private val cityRepository: CityRepository): ViewModelProvider.NewInstanceFactory()  {

    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return CitiesViewModel(cityRepository) as T
    }

}