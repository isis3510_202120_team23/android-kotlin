package com.example.trapp.ui.budget

import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import com.example.trapp.R
import com.example.trapp.data.model.*
import com.example.trapp.formatters.ValueFormatterY
import com.example.trapp.formatters.XAxisFormatter
import com.example.trapp.ui.fragments.navbar.NavbarFragment
import com.example.trapp.ui.plan.PlansMarioViewModel
import com.example.trapp.ui.user.UsersViewModel
import com.example.trapp.ui.usergroup.UserGroupsViewModel
import com.example.trapp.utilities.InjectorUtils
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import kotlinx.coroutines.*
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class BudgetActivity : AppCompatActivity() {
    // Cuál es el el promedio gastado diariamente del budget por el usuario?

    private var noData = false
    private var progr = 0

    //Guardar esto
    private var expenses = mutableListOf<DailyExpense>()

    //Guardar esto
    private var totalBudget = 0.0
    private var budget = Budget(1, 666.666, 666L)

    //Guardar esto
    private var totalExpenses = 0.0

    private var timeLoadingInit = 0L
    private var timeLoadingEnd = 0L


    private var activePlan = PlanMario("null")
    private var userGroup = UserGroup("null")
    private var user = User("null")
    private val db = Firebase.firestore

    var cm: ConnectivityManager? = null
    var activeNetwork: NetworkInfo? = null

    private val currentUser = Firebase.auth.currentUser
    private var plansList: MutableList<PlanMario> = emptyList<PlanMario>().toMutableList()
    val trace = Firebase.performance.newTrace("Loading time budget Activity")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trace.start()
        timeLoadingInit = Calendar.getInstance().timeInMillis

        setContentView(R.layout.activity_budget)
        val progressBar = findViewById<ProgressBar>(R.id.toploadingbar)
        progressBar.visibility = View.VISIBLE
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                val fragment = NavbarFragment()
                replace(R.id.navbar_fragment, fragment, "NavbarFragment")
            }
        }


        lifecycleScope.launch {
            initializeUI()
            if (noData) {
                setContentView(R.layout.no_internet_screen)
                if (savedInstanceState == null) {
                    supportFragmentManager.commit {
                        setReorderingAllowed(true)
                        val fragment = NavbarFragment()
                        replace(R.id.navbar_fragment, fragment, "NavbarFragment")
                    }
                }
                val retryButton = findViewById<Button>(R.id.RetryButton)
                retryButton.setOnClickListener {

                    if (getInternet()) {
                        lifecycleScope.launch {
                            setContentView(R.layout.activity_budget)
                            val progressBar = findViewById<ProgressBar>(R.id.toploadingbar)
                            progressBar.visibility = View.VISIBLE
                            if (savedInstanceState == null) {
                                supportFragmentManager.commit {
                                    setReorderingAllowed(true)
                                    val fragment = NavbarFragment()
                                    replace(R.id.navbar_fragment, fragment, "NavbarFragment")
                                }
                            }
                            initializeUI()
                            progressBar.visibility = View.INVISIBLE
                        }
                    } else {

                        val view: View = window.decorView.rootView
                        val snackbar =
                            Snackbar.make(view, R.string.nointernettext, Snackbar.LENGTH_LONG)
                                .setAnchorView(R.id.navbar_fragment)
                        snackbar.show()
                    }

                }
            }
            progressBar.visibility = View.INVISIBLE
        }
    }

    override fun onStart() {
        super.onStart()
        trace.stop()
        timeLoadingEnd = Calendar.getInstance().timeInMillis
        writeLoadingTime("Budget", timeLoadingEnd - timeLoadingInit)
    }

    private fun writeLoadingTime(activity: String, time: Long) {

        val userTime = hashMapOf(
            "activity" to activity,
            "time" to time
        )
        db.collection("loadingTime").add(userTime)
    }

    private fun updateProgressBar() {
        val progress_bar = findViewById<ProgressBar>(R.id.progress_bar)
        progress_bar.progress = progr
        val text_view_progress = findViewById<TextView>(R.id.text_view_progress)
        text_view_progress.text = "$progr%"
    }

    fun modifyBarChartAndProgressBar() {
        val chart = findViewById<BarChart>(R.id.bargraph)

        val entries: MutableList<BarEntry> = ArrayList()

        for (expense in expenses) {
            if (expense.amount != null && expense.dayNumber != null) {
                entries.add(BarEntry(expense.dayNumber.toFloat(), expense.amount.toFloat()))
                totalExpenses += expense.amount.toDouble()
            }
        }

        //Bar Chart
        val set = BarDataSet(entries, "Expenses")
        set.color = Color.rgb(237, 216, 61)
        val data = BarData(set)
        data.barWidth = 0.9f // set custom bar width
        chart.data = data
        chart.setFitBars(true) // make the x-axis fit exactly all bars
        chart.xAxis.valueFormatter = XAxisFormatter()
        chart.axisRight.valueFormatter = ValueFormatterY()
        chart.axisLeft.valueFormatter = ValueFormatterY()

        val description: Description = chart.description
        description.isEnabled = false

        chart.invalidate() // refresh

        //Progress Bar
        val format = NumberFormat.getCurrencyInstance()
        val totalExpensesFormatted = format.format(totalExpenses)
        val totalBudgetFormatted = format.format(totalBudget)
        val text = findViewById<TextView>(R.id.textView3)
        text.text = "$totalExpensesFormatted / $totalBudgetFormatted"

        val percentage = (totalExpenses / totalBudget) * 100

        progr = percentage.toInt()

        updateProgressBar()
    }

    suspend fun getTotalBudget() {
        totalBudget = userGroup.totalBudget!! / userGroup.numberMembers!!

        val budgetsFactory = InjectorUtils.provideBudgetsViewModelFactory(applicationContext)
        val budgetsViewModel =
            ViewModelProviders.of(this, budgetsFactory).get(BudgetViewModel::class.java)

        val deferred: Deferred<Boolean> = CoroutineScope(Dispatchers.IO).async {

            budgetsViewModel.insertBudget(
                Budget(
                    0,
                    totalBudget,
                    Calendar.getInstance().timeInMillis
                )
            )
            true
        }
        if (deferred.await()) {
            getExpenses()
        }
    }

    fun findActivePlan() {
        if (userGroup.plans != null) {
            var i = 0
            var found = false
            while (i < userGroup.plans!!.size && !found) {
                val planId = userGroup.plans!![i]
                var j = 0
                while (j < plansList.size && !found) {
                    val candidatePlan = plansList[j]
                    if (candidatePlan.firebaseId == planId && candidatePlan.active == true) {
                        activePlan = candidatePlan
                        found = true
                    }
                    j++
                }
                i++
            }
        }

    }

    fun getExpenses() {
        val dailyExpenses = user.dailyExpenses
        if (dailyExpenses != null) {

            for (expense in dailyExpenses) {
                //En algún lado, el usuario debería escoger el plan actual o quizá que se ponga cuando se cree el plan, por ahora es un mockup 2QYiS4sT4fVc38NgIG4s
                if (expense.planId == activePlan.firebaseId) {
                    expenses.add(
                        DailyExpense(
                            expense.id,
                            expense.amount,
                            expense.dayNumber,
                            expense.planId,
                            null
                        )
                    )
                }
            }
            val dailyExpensesFactory =
                InjectorUtils.provideDailyExpensesViewModelFactory(applicationContext)
            val dailyExpensesViewModel =
                ViewModelProviders.of(this, dailyExpensesFactory)
                    .get(DailyExpenseViewModel::class.java)

            CoroutineScope(Dispatchers.IO).launch {
                for (expense in expenses) {
                    dailyExpensesViewModel.insertDailyExpense(
                        DailyExpense(
                            expense.id,
                            expense.amount,
                            expense.dayNumber,
                            expense.planId,
                            budget.id
                        )
                    )
                }
            }
        }
    }

    private suspend fun initializeUI() {
        val budgetsFactory = InjectorUtils.provideBudgetsViewModelFactory(applicationContext)
        val budgetsViewModel =
            ViewModelProviders.of(this, budgetsFactory).get(BudgetViewModel::class.java)

        budgetsViewModel.getBudgets().observe(this, { budgets ->

            totalBudget = budgets[0].totalBudget
            budget = budgets[0]
        })

        if (totalBudget === 0.0) {

            if (!getInternet()) {
                noData = true
            } else {
                val userFactory = InjectorUtils.provideUsersViewModelFactory()
                val userViewModel =
                    ViewModelProviders.of(this, userFactory).get(UsersViewModel::class.java)
                if (currentUser != null) {
                    userViewModel.getUserById(currentUser.uid).observe(this, { userFound ->
                        user = userFound
                    })
                }

                val plansFactory = InjectorUtils.providePlansMarioViewModelFactory()
                val plansViewModel =
                    ViewModelProviders.of(this, plansFactory).get(PlansMarioViewModel::class.java)

                plansViewModel.getPlans().observe(this, { plans ->
                    plansList.clear()
                    plans.forEach { plan ->
                        plansList.add(plan)
                    }
                })
                val userGroupFactory = InjectorUtils.provideUserGroupsViewModelFactory()
                val userGroupViewModel =
                    ViewModelProviders.of(this, userGroupFactory)
                        .get(UserGroupsViewModel::class.java)

                userGroupViewModel.getUserGroupById("2ETlR8ptvc1ZCoQmyAXa")
                    .observe(this, { usergroup ->
                        userGroup = usergroup
                    })

                findActivePlan()
                getTotalBudget()

                modifyBarChartAndProgressBar()
            }


        } else {

            val dailyExpensesFactory =
                InjectorUtils.provideDailyExpensesViewModelFactory(applicationContext)
            val dailyExpensesViewModel =
                ViewModelProviders.of(this, dailyExpensesFactory)
                    .get(DailyExpenseViewModel::class.java)

            dailyExpensesViewModel.getDailyExpensesByBudgetId(budget.id)
                .observe(this, { dailyExpenses ->
                    expenses = dailyExpenses.toMutableList()
                })
            modifyBarChartAndProgressBar()
        }


    }

    private fun getInternet(): Boolean {
        cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        activeNetwork = cm!!.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        return isConnected
    }

}