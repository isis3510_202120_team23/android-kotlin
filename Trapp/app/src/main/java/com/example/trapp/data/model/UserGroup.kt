package com.example.trapp.data.model

data class UserGroup(
    val firebaseId: String,
    var numberMembers: Long? = 0,
    val plans: List<String>? = null,
    val totalBudget: Double? = 0.0
)