package com.example.trapp.ui.budget

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.place.BudgetRepository
import com.example.trapp.data.place.DailyExpenseRepository

class DailyExpensesViewModelFactory (private val dailyExpenseRepository: DailyExpenseRepository): ViewModelProvider.NewInstanceFactory() {


    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return DailyExpenseViewModel(dailyExpenseRepository) as T
    }

}
