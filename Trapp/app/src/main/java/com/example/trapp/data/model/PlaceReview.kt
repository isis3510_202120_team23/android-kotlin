package com.example.trapp.data.model;

data class PlaceReview(
    val calification: Long,
    val comment: String,
    val author: String
)
