package com.example.trapp.ui.trip

import androidx.lifecycle.ViewModel
import com.example.trapp.data.trip.TripRepository

class TripsViewModel(private val tripRepository: TripRepository) : ViewModel(){

     suspend fun getTrips()=tripRepository.getTrips()
    /* fun getOriginLinkTrip() = tripRepository.getOriginLinkTrip()
     fun getDestinationLinkTrip() = tripRepository.getDestinationLinkTrip()
    fun getTripDestination(tripId:String) = tripRepository.getTripDestination(tripId)
    suspend fun getTripDestinationIdAsync(tripId:String?) = tripRepository.getTripDestinationIdAsync(tripId)*/
}