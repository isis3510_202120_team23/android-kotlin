package com.example.trapp.ui.plan

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.compose.material.Snackbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trapp.R
import com.example.trapp.data.model.Place
import com.example.trapp.data.model.Trip
import com.example.trapp.databinding.ActivityPlanBinding
import com.example.trapp.databinding.NavbarBinding
import com.example.trapp.databinding.NoInternetScreenBinding
import com.example.trapp.ui.trip.TripsViewModel
import com.example.trapp.ui.budget.BudgetInput
import com.example.trapp.ui.fragments.navbar.NavbarFragment
import com.example.trapp.ui.place.GPSAdapter
import com.example.trapp.ui.place.PlaceViewModel
import com.example.trapp.utilities.InjectorUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.google.firebase.perf.metrics.AddTrace
import kotlinx.coroutines.*
import java.util.*
import com.example.trapp.utilities.Connectivity
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG

class PlanActivity : AppCompatActivity(), BudgetInput.ExampleDialogListener,
    LocationListener {

    private lateinit var binding: ActivityPlanBinding
    private lateinit var offlinebinding: NoInternetScreenBinding
    private lateinit var navbarbinding: NavbarBinding

    private val db = Firebase.firestore

    private val itemRenderLimit = if (Runtime.getRuntime().availableProcessors() <= 4) 8 else 16

    private val timeSpent = 0
    private var timeLoadingInit = 0L
    private var timeLoadingEnd = 0L
    private var timeSelectInit = 0L
    private var timeSelectEnd = 0L
    private lateinit var initialTime: Calendar
    var tripsAdapter: PlansAdapter? = null
    private lateinit var auth: FirebaseAuth
    private var budget = 0L
    private var radio = 0.05
    private var listaTrips: MutableList<Trip> = emptyList<Trip>().toMutableList()
    private var listaPlaces: MutableList<Place> = emptyList<Place>().toMutableList()
    private var budgetCompliantTrips: MutableList<Trip> = emptyList<Trip>().toMutableList()
    private var gpsCompliantPlaces: MutableList<Place> = emptyList<Place>().toMutableList()
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2
    private var lat = 0.0
    private var lon = 0.0
    var cm: ConnectivityManager? = null
    var activeNetwork: NetworkInfo? = null
    private var noInternet: String =
        "Sorry, there is no internet connection. Make sure that WiFi or mobile data is turned on and try again. "
    val trace = Firebase.performance.newTrace("Loading time plan Activity")

    //var roomDB: AppDatabase? = null
    var lPlace: Boolean = false
    var l1Place: Boolean = false
    var lTrip: Boolean = false
    val earthRadiusKm: Double = 6372.8
    private var isNetworkConnected = false


    private fun getInternet(): Boolean {
        var connectivity = Connectivity.getInstance()
        return connectivity.getInternet(applicationContext)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlanBinding.inflate(layoutInflater)
        offlinebinding = NoInternetScreenBinding.inflate(layoutInflater)
        navbarbinding = NavbarBinding.inflate(layoutInflater)

        trace.start()
        timeLoadingInit = Calendar.getInstance().timeInMillis
        //ASk for the permission
        val requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {

                } else {

                }
            }
        val permissionCheck =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            requestPermissionLauncher.launch(
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
        val view = binding.root
        setContentView(view)

        lifecycleScope.launch {
            getLocation()


            //Log.d("lista vacia?", listatrips.toString())
            initializeUi(savedInstanceState)


        }
        val btnGPS = binding.button

        btnGPS.setOnClickListener {

            getLocation()
            com.google.android.material.snackbar.Snackbar.make(
                window.decorView.rootView,
                "Places are already actualized",
                com.google.android.material.snackbar.Snackbar.LENGTH_LONG
            )
                .setAnchorView(R.id.navbar_fragment).show()
        }

        val budgetRecycler: RecyclerView = binding.budgetRecycler
        tripsAdapter = PlansAdapter(budgetCompliantTrips)
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        budgetRecycler.layoutManager = layoutManager
        budgetRecycler.adapter = tripsAdapter

        //Version GPS
        val gpsRecycler: RecyclerView = binding.GPSRecycler
        val placesAdapter = GPSAdapter(gpsCompliantPlaces)
        val layoutGPSManager = LinearLayoutManager(this)
        layoutGPSManager.orientation = LinearLayoutManager.HORIZONTAL
        gpsRecycler.layoutManager = layoutGPSManager
        gpsRecycler.adapter = placesAdapter

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                val fragment = NavbarFragment()
                replace(R.id.navbar_fragment, fragment, "NavbarFragment")
            }
        }


        val editButton: AppCompatButton? = binding.editButton
        editButton?.setOnClickListener {

            if (getInternet()) {
                openDialog()
            } else {
                Toast.makeText(
                    this,
                    noInternet,
                    Toast.LENGTH_LONG
                ).show()
            }
        }


    }

    @SuppressLint("WrongConstant")
    @AddTrace(name = "InitializeUi plan activity", enabled = true /* optional */)
    private suspend fun initializeUi(savedInstanceState: Bundle?) {

            val factory = InjectorUtils.provideTripsViewModelFactory(applicationContext)
            val viewModel =
                ViewModelProviders.of(this, factory).get(TripsViewModel::class.java)

            viewModel.getTrips().observe(this, { trips ->
                listaTrips.clear()
                listaTrips.addAll(trips)
                if(listaTrips.isNotEmpty()) {
                    reloadBudgetTrips()
                } else {
                    //Log.d("ninguna de las dos :(", "----------------------------------------")
                    val offlineview = offlinebinding.root
                    setContentView(offlineview)
                    if (savedInstanceState == null) {
                        supportFragmentManager.commit {
                            setReorderingAllowed(true)
                            val fragment = NavbarFragment()
                            replace(R.id.navbar_fragment, fragment, "NavbarFragment")
                        }
                    }
                    val retryButton: AppCompatButton = offlinebinding.RetryButton
                    retryButton.setOnClickListener {
                        if (getInternet()) {
                            //Log.d("Entro a internet :p","-------------------------------------")
                            finish()
                            startActivity(intent)
                        } else {
                            val view: View = window.decorView.rootView
                            val snackbar = com.google.android.material.snackbar.Snackbar.make(
                                view,
                                R.string.nointernettext,
                                com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE
                            )
                                .setAnchorView(R.id.navbar_fragment)
                            snackbar.show()
                        }
                    }
                }
            }
            )

            val factoryPlace = InjectorUtils.providePlacesViewModelFactory(applicationContext)
            val viewModelPlace =
                ViewModelProviders.of(this, factoryPlace).get(PlaceViewModel::class.java)

            viewModelPlace.getPlaces().observe(this, { places ->
                Log.d("ACTIVIDAD", "" + places)
                listaPlaces.clear()
                listaPlaces.addAll(places)
                if (listaPlaces.isNotEmpty()) {
                    reloadGPSTrips()
                }
                else {
                    //Log.d("ninguna de las dos :(", "----------------------------------------")
                    val offlineview = offlinebinding.root
                    setContentView(offlineview)
                    if (savedInstanceState == null) {
                        supportFragmentManager.commit {
                            setReorderingAllowed(true)
                            val fragment = NavbarFragment()
                            replace(R.id.navbar_fragment, fragment, "NavbarFragment")
                        }
                    }
                    val retryButton: AppCompatButton = offlinebinding.RetryButton
                    retryButton.setOnClickListener {
                        if (getInternet()) {
                            //Log.d("Entro a internet :p","-------------------------------------")
                            finish()
                            startActivity(intent)
                        } else {
                            val view: View = window.decorView.rootView
                            val snackbar = com.google.android.material.snackbar.Snackbar.make(
                                view,
                                R.string.nointernettext,
                                com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE
                            )
                                .setAnchorView(R.id.navbar_fragment)
                            snackbar.show()
                        }
                    }
                }
            })
            //loadCards()
        if (!getInternet()) {

            com.google.android.material.snackbar.Snackbar.make(
                window.decorView.rootView,
                "You dont have connection you are using local data that may not be updated",
                LENGTH_LONG
            ).show()
        }


        }






    override fun onStart() {
        super.onStart()

        initialTime = Calendar.getInstance()
        timeSelectInit = Calendar.getInstance().timeInMillis
        timeLoadingEnd = Calendar.getInstance().timeInMillis
        writeLoadingTime("Plan", timeLoadingEnd - timeLoadingInit)
        trace.stop()
    }

    override fun onStop() {
        super.onStop()
        val finalTime = Calendar.getInstance()
        val millis1: Long = initialTime.timeInMillis
        val millis2: Long = finalTime.timeInMillis

        // Calculate difference in milliseconds
        val diff = millis2 - millis1

        // Calculate difference in seconds
        val timeSpent = diff / 1000

        //save to database...

        writeNewTime("Plan", timeSpent)
        timeSelectEnd = tripsAdapter?.getTime()!!
        if (timeSelectEnd != 0L) {
            writeSelectingTime(timeSelectEnd - timeSelectInit)
        }

    }

    private fun writeNewTime(activity: String, time: Long) {

        val userTime = hashMapOf(
            "activity" to activity,
            "time" to time
        )
        db.collection("userTime").add(userTime)
    }

    private fun writeLoadingTime(activity: String, time: Long) {

        val userTime = hashMapOf(
            "activity" to activity,
            "time" to time
        )
        Log.d("loading time", userTime.toString())
        db.collection("loadingTime").add(userTime)
    }

    private fun writeSelectingTime(time: Long) {

        val userTime = hashMapOf(
            "time" to time
        )
        db.collection("selectTime").add(userTime)
    }

    private fun openDialog() {

        if (getInternet()) {
            val exampleDialog = BudgetInput()
            exampleDialog.show(supportFragmentManager, "example dialog")
        } else {
            Toast.makeText(
                this,
                noInternet,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun applyTexts(pBudget: String?) {

        if (getInternet()) {
            budget = if (pBudget != null) {
                try {
                    pBudget.toLong()
                } catch (e: Exception) {
                    0L
                }

            } else {
                0L
            }
            reloadBudgetTrips()
        } else {
            Toast.makeText(
                this,
                noInternet,
                Toast.LENGTH_LONG
            ).show()
        }
    }


    private fun reloadBudgetTrips() {

        var budgetRecycler: RecyclerView = binding.budgetRecycler
        budgetCompliantTrips.clear()
        if (budget <= 0) {

            for (trip in listaTrips) {
                budgetCompliantTrips.add(trip)
            }
        } else {
            for (trip in listaTrips) {
                if (trip.price <= budget) {
                    budgetCompliantTrips.add(trip)
                }
            }
        }
        budgetRecycler.adapter?.notifyDataSetChanged()
    }


    fun stopLoading() {
        if (lTrip && lPlace) {
            binding.toploadingbar.visibility = View.INVISIBLE
        }
    }

    private fun reloadGPSTrips() {
        Log.d("RELOADTRIPS", "" + listaPlaces)
        var gpsRecycler: RecyclerView = binding.GPSRecycler
        gpsCompliantPlaces.clear()

        if (radio <= 0) {
            for (place in listaPlaces) {
                gpsCompliantPlaces.add(place)
            }
            radio = 0.05
        } else {
            for (place in listaPlaces) {
                //         if (haversine(place.latitude.toDouble(), place.longitude.toDouble()) <= 4) {
                gpsCompliantPlaces.add(place)
                //        }
            }
        }
        gpsRecycler.adapter?.notifyDataSetChanged()


    }

    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if ((ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                locationPermissionCode
            )
        }
        var location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)

    }

    override fun onLocationChanged(location: Location) {
        //Log.d("Location", location.toString() + "------------------------")
        lat = location.latitude
        lon = location.longitude
        //l1Place = true
        stopLoading()
        reloadGPSTrips()
        locationManager.removeUpdates(this)


        if (location != null) {
            lat = location.latitude
            lon = location.longitude
            reloadGPSTrips()
        }

    }
}



