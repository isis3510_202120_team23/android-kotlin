package com.example.trapp.data.dailyexpense

import androidx.room.*
import com.example.trapp.data.model.DailyExpense
import com.example.trapp.data.model.Place


@Dao
interface DailyExpenseRoomDAO {
    @Query("SELECT * FROM daily_expense")
    suspend fun getAll(): List<DailyExpense>

    @Query("SELECT * FROM daily_expense WHERE budget_id=:budget_id")
    suspend fun getByBudgetId(budget_id: Long): List<DailyExpense>

    @Insert
    suspend fun insert(dailyExpense: DailyExpense)

    @Insert
    fun insertAll(vararg dailyExpense: DailyExpense)

    @Update
    suspend fun update(dailyExpense: DailyExpense)

    @Delete
    suspend fun delete(dailyExpense: DailyExpense)
}