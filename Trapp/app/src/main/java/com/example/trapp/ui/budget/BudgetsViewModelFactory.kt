package com.example.trapp.ui.budget

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.place.BudgetRepository

class BudgetsViewModelFactory (private val budgetRepository: BudgetRepository): ViewModelProvider.NewInstanceFactory() {


    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return BudgetViewModel(budgetRepository) as T
    }

}
