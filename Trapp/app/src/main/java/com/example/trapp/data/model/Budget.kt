package com.example.trapp.data.model

import androidx.room.*

@Entity(tableName = "budget")
data class Budget(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @ColumnInfo(name = "total_budget")
    val totalBudget: Double,
    @ColumnInfo(name = "save_date")
    val saveDate: Long
)