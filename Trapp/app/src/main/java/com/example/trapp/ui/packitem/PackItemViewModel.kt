package com.example.trapp.ui.packitem

import android.util.ArrayMap
import android.util.Pair
import androidx.lifecycle.*
import com.example.trapp.data.packitem.PackItemRepository
import com.example.trapp.data.model.PackItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PackItemViewModel(private val packItemRepository: PackItemRepository) : ViewModel() {


    private val packItemMap = ArrayMap<String, PackItem>()

    // suggestedPackItems<MutablePair<Boolean,PackItem>>

    // <tripId, List< isSuggested? , PackItem > >
    private val displayPackItemsMap = ArrayMap<String, List<Pair<Boolean, PackItem>>>()

    private val suggestedTripPackItemMap = ArrayMap<String, List<String>>()
    private val suggestedCityPackItemMap = ArrayMap<String, List<String>>()

    private val displayPackItems =
        MutableLiveData<ArrayMap<String, List<Pair<Boolean, PackItem>>>>()

    init {
        displayPackItems.value = displayPackItemsMap
    }




    private suspend fun getPackItems() {
        if (packItemMap.isEmpty()) {
            val add = packItemRepository.getPackItems().associateBy { it.itemId }
            packItemMap.putAll(add)
        }
    }


    private suspend fun getSuggestedTripPackItems(tripId: String) {
        if (!suggestedTripPackItemMap.containsKey(tripId)) {
            val suggestedPackItems = packItemRepository.getSuggestedTripPackItems(tripId)
            if(suggestedPackItems.isNotEmpty()){
            suggestedTripPackItemMap[tripId] = suggestedPackItems}
        }
    }

    private suspend fun getSuggestedCityPackItems(tripId: String) {
        if (!suggestedCityPackItemMap.containsKey(tripId)) {
            val suggestedPackItems = packItemRepository.getSuggestedCityPackItems(tripId)
            if(suggestedPackItems.isNotEmpty()){
            suggestedCityPackItemMap[tripId] = suggestedPackItems}
        }
    }

    private fun getNotSuggestedItems(tripId: String?): List<PackItem> {
        val list = ArrayList<PackItem>()

        if (tripId != null) {
            val suggestedTripItems = suggestedTripPackItemMap[tripId] ?: emptyList()
            val suggestedCityItems = suggestedCityPackItemMap[tripId] ?: emptyList()
            packItemMap.filter {
                !suggestedTripItems.contains(it.key) && !suggestedCityItems.contains(it.key)
            }.forEach {
                list.add(it.value)
            }
        } else {
            packItemMap.forEach { entry -> list.add(entry.value) }
        }
        return list
    }


    fun getDisplayPackItems(tripId: String?): LiveData<ArrayMap<String, List<Pair<Boolean, PackItem>>>> {

        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                if (!displayPackItemsMap.containsKey(tripId)) {
                    getPackItems()
                    if (tripId != null) {
                        getSuggestedTripPackItems(tripId)
                        getSuggestedCityPackItems(tripId)
                    }
                    val notSuggestedItems = getNotSuggestedItems(tripId)
                    val fullList = ArrayList<Pair<Boolean, PackItem>>()
                    if (suggestedTripPackItemMap.containsKey(tripId)) {
                        suggestedTripPackItemMap[tripId]!!.forEach {
                            fullList.add(Pair(true, packItemMap[it]!!))
                        }
                    }
                    if (suggestedCityPackItemMap.containsKey(tripId)) {
                        suggestedCityPackItemMap[tripId]!!.forEach {
                            fullList.add(Pair(true, packItemMap[it]!!))
                        }
                    }
                    fullList.addAll(notSuggestedItems.map { packItem -> Pair(false, packItem) })
                    displayPackItemsMap[tripId] = fullList

                    displayPackItems.postValue(displayPackItemsMap)
                }
            }
        }
        return displayPackItems as LiveData<ArrayMap<String, List<Pair<Boolean, PackItem>>>>

    }

}