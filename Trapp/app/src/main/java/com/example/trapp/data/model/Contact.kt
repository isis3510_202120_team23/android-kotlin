package com.example.trapp.data.model

import android.graphics.Bitmap
import android.net.Uri
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Contact
   (@PrimaryKey(autoGenerate = true) val idSQL: Long,
    var id:String,
    @ColumnInfo(name = "name")var name: String? = null,
    @ColumnInfo(name = "mobile_number")var mobileNumber: String? = null,
    @ColumnInfo(name = "save_date") var saveDate: Long?=null) {


}
