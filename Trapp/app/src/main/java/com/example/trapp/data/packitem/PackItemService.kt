package com.example.trapp.data.packitem

import android.util.Log
import com.example.trapp.data.trip.TripDAO
import com.example.trapp.data.city.CityService
import com.example.trapp.data.model.PackItem
import com.example.trapp.data.model.Review
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class PackItemService {

    private val db = Firebase.firestore
    private val items = "items"
    private val cityService: CityService = CityService.newInstance()
    private val tripDAO: TripDAO = TripDAO.newInstance()

    companion object {
        fun newInstance() = PackItemService()
    }

    suspend fun getAll():List<PackItem>{
        return withContext(Dispatchers.IO){

            try {
                val itemList = ArrayList<PackItem>()
                val citiesRef = db.collection(items)
                val query = citiesRef.get().await()
                for (document in query) {
                    val item = PackItem(document.id, document.data?.getValue("name") as String)
                    itemList.add(item)
                }
                itemList as List<PackItem>
            }
            catch (e: Exception){
                Log.d("PackItemService", "getAll: ${e.stackTraceToString()}")
                emptyList<PackItem>()
            }
        }
    }

    suspend fun getSuggestedTripPackItems(tripId:String): List<String> {
        return withContext(Dispatchers.IO){
            try {
                tripDAO.getSuggestedItemsIdsTripAsync(tripId) as List<String>
            } catch (e: Exception) {
                emptyList<String>()
            }

        }
    }

    suspend fun getSuggestedCityPackItems(tripId:String):List<String>{
        return withContext(Dispatchers.IO){
            try {
                val destinationId: String? = tripDAO.getTripDestinationIdAsync(tripId)
                if (destinationId != null) {
                    getCityPackItems(destinationId)
                }
                else{
                    emptyList<String>()
                }
            } catch (e: Exception) {
                emptyList<String>()
            }
        }

    }


    private suspend fun getCityPackItems(cityId:String):List<String>{
        return withContext(Dispatchers.IO){
            try {
                val items = ArrayList<String>()
                val tripids: List<String>? =
                    tripDAO.getDestinationLinkTrip().value?.get(cityId)

                if (tripids != null) {
                    for (tripid in tripids) {
                        val reviews: MutableList<Review> = tripDAO.getTripReviewsAsync(tripid)
                        var sum = 0.0
                        for (review in reviews) {
                            sum += review.calification
                        }
                        val avg: Double = sum / (reviews.size.toDouble())
                        if (avg > 3.5) {

                            items.addAll(tripDAO.getSuggestedItemsIdsTripAsync(tripid))
                        }
                    }
                }
                items.addAll(cityService.getSuggestedItemsIdsCityAsync(
                    cityId
                ))
                Log.d("PackItemService", "getSuggestedCityPackItems: $items")
                items
            } catch (e: Exception) {
                Log.d("PackItemService", "getSuggestedCityPackItems: ${e.stackTraceToString()}")
                emptyList<String>()
            }
        }
    }

    fun createPackItem(packItemId: String, name: String) {
        val data = hashMapOf(
            "name" to name
        )
        db.collection(items).document(packItemId).set(data).addOnSuccessListener {
            Log.d(items, "DocumentSnapshot written with ID: $packItemId")
        }
            .addOnFailureListener { e ->
                Log.w(items, "Error adding document", e)
            }
    }

}