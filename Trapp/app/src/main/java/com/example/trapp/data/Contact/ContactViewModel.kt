package com.example.trapp.data.Contact

import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.trapp.data.place.PlaceRepository

class ContactViewModel (private val contactRepository: ContactRepository, private val context: Context) : ViewModel(){

    suspend fun getContacts()=contactRepository.getContacts(context)
}