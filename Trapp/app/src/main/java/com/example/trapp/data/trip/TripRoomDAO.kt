package com.example.trapp.data.trip

import androidx.room.*
import com.example.trapp.data.model.Trip


@Dao
    interface TripRoomDAO {
        @Query("SELECT * FROM trip")
        fun getAll(): List<Trip>

        @Query("SELECT * FROM trip WHERE id IN (:tripIds)")
        fun loadAllByIds(tripIds: String): List<Trip>

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        fun insertAll(vararg trip: Trip)

        @Delete
        fun delete(trip: Trip)

        @Query("DELETE FROM trip")
        fun deleteAll()
    }