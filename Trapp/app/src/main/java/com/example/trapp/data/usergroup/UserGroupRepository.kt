package com.example.trapp.data.usergroup

class UserGroupRepository private constructor(private val UserGroupDAO: UserGroupDAO) {

    suspend fun getUserGroups() = UserGroupDAO.getUserGroups()
    suspend fun getUserGroupById(UserGroupId: String) = UserGroupDAO.getUserGroupById(UserGroupId)

    companion object {
        @Volatile
        private var instance: UserGroupRepository? = null

        fun getInstance(UserGroupDAO: UserGroupDAO) =
            instance ?: synchronized(this) {
                instance ?: UserGroupRepository(UserGroupDAO).also { instance = it }
            }
    }


}