package com.example.trapp.ui.plan

import android.content.Context
import android.content.Intent
import android.icu.text.NumberFormat
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat.startActivity
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trapp.R
import com.example.trapp.data.model.Trip
import com.example.trapp.databinding.TripCardBinding
import com.example.trapp.ui.packitem.PackItemActivity
import java.util.*

class PlansAdapter (private val trips:List<Trip>): RecyclerView.Adapter<PlansAdapter.ViewHolder>(){
    private var timeSelect=0L

    inner class ViewHolder(tripBinding: TripCardBinding) : RecyclerView.ViewHolder(tripBinding.root) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val planButton: AppCompatButton =   tripBinding.planButton
        val planImage: ImageView=   tripBinding.planImagePreview
        val planPrice: TextView=    tripBinding.planPrice
        val context: Context =  tripBinding.root.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val tripView = TripCardBinding.inflate(LayoutInflater.from(parent.context),
            parent, false)
        // Return a new holder instance
        return ViewHolder(tripView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val trip: Trip = trips[position]
        // Set item views based on your views and data model
        val button = viewHolder.planButton
        val image = viewHolder.planImage
        val price = viewHolder.planPrice

        val imgUrl: String = trip.img
        //Caching strategy Glide
        Glide.with(image).load(imgUrl).placeholder(R.mipmap.city3_foreground).into(image)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val numberFormat = NumberFormat.getCurrencyInstance()
            numberFormat.maximumFractionDigits = 0
            val convert:String = numberFormat.format(trip.price)
            price.text=convert
        } else {
            price.text= trip.price.toString()
        }

        button.setOnClickListener{
            timeSelect= Calendar.getInstance().timeInMillis
            val bundle: Bundle = bundleOf("trip" to trip.id)
            val intent = Intent(
                button.context,
                PackItemActivity::class.java
            )
            intent.putExtras(bundle)
            startActivity(button.context,intent,bundle)
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return trips.size
    }
    fun getTime(): Long {
        return timeSelect
    }

}