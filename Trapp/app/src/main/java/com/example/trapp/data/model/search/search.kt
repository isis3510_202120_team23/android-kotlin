package com.example.trapp.data.model.search

data class Search(
    val hadResults: Boolean,
    val term: String,
    val timestamp: Long
)
