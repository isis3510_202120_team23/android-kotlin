package com.example.trapp.data.plan


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.util.*


class PlanDAO {
    private val db = Firebase.firestore
    private val collectionName = "plans"
    private val planList = mutableListOf<Plan>()
    private var plan = Plan()

    private val plans = MutableLiveData<List<Plan>>()
    private val planLive = MutableLiveData<Plan>()

    init {
        plans.value = planList
        planLive.value = plan
    }

    companion object {
        fun newInstance() = PlanDAO()
    }

    suspend fun getAllPlansAsync(): MutableList<Plan> = withContext(Dispatchers.IO) {
        return@withContext try {
            val data = db.collection(collectionName)
                .get()
                .await()

            for (document in data.documents) {
               var plan = document.toObject(Plan::class.java)!!
                planList.add(plan)
            }
            planList
        } catch (e: Exception) {
            planList
        }
    }


    suspend fun getPlans(): LiveData<List<Plan>> {
        getAllPlansAsync()
        return plans
    }
    suspend fun createPlan(plan: Plan){
        plan.month= Calendar.MONTH
        db.collection(collectionName).add(plan)
    }

 /*   suspend fun getPlanById(planId: String): LiveData<Plan> {
        getPlanByIdAsync(planId)

        println("-------------------------------" + planLive.value)
        planLive.value = plan
        return planLive
    }*/

}
