package com.example.trapp.ui.search

import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.trapp.data.model.PackItem
import com.example.trapp.data.model.Place
import com.example.trapp.data.model.Trip
import com.example.trapp.data.model.User
import com.example.trapp.data.model.search.Search
import com.example.trapp.data.packitem.PackItemRepository
import com.example.trapp.data.place.PlaceRepository
import com.example.trapp.data.plan.PlanMarioRepository
import com.example.trapp.data.search.SearchService
import com.example.trapp.data.trip.TripRepository
import com.example.trapp.data.user.UserRepository
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.google.firebase.perf.metrics.AddTrace
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.min

class SearchViewModel(
    private val tripsRepository: TripRepository,
    private val placesRepository: PlaceRepository,
    private val packItemRepository: PackItemRepository,

    ) : ViewModel() {

    private val dispatcher = Dispatchers.Default

    private val maxDamerauLevenshteinDistance: Int = 8

    private var searchTerm: String = ""

    // IDEA: usar maps para hacer key= searchterm usado & value= lista de resultados

    private val placesResults = mutableListOf<Place>()

    private val tripsResults = mutableListOf<Trip>()

    private val itemsResults = mutableListOf<PackItem>()

    private val usersResults = mutableListOf<User>()

    // false= loading , true= loaded
    private val searchingStatus = false

    // livedatas

    private val searchTermLiveData = MutableLiveData<String>()

    // listas con los ids de objetos resultado
    // TODO: estar pendiente, podria cambiar por objetos o maps

    private val placesResultsLiveData = MutableLiveData<List<Place>>()

    private val tripsResultsLiveData = MutableLiveData<List<Trip>>()

    private val itemsResultsLiveData = MutableLiveData<List<PackItem>>()

    private val usersResultsLiveData = MutableLiveData<List<User>>()

    private val searchingStatusLive: MutableLiveData<Boolean> = MutableLiveData()

    init {
        searchTermLiveData.value = searchTerm
        //    plansResultsLiveData.value = plansResults
        placesResultsLiveData.value = placesResults
        tripsResultsLiveData.value = tripsResults
        itemsResultsLiveData.value = itemsResults
        usersResultsLiveData.value = usersResults
        searchingStatusLive.value = searchingStatus
    }

    // actually perform the search in the current fragment
    @AddTrace(name = "SearchViewModel_searchFunction")
    suspend fun search(where: Int) = withContext(dispatcher) {
        searchingStatusLive.postValue(true)
        Log.d("SearchViewModel", "Searching...")
        when (where) {
            PLACESPOSITION -> {

                searchInPlaces()

            }
            TRIPSPOSITION -> {

                searchInTrips()

            }
            ITEMSPOSITION -> {

                searchInItems()
            }
            else -> {

                Log.d("SearchViewModel", "????????????")
            }
        }
    }

    fun getSearchTerm(): LiveData<String> {
        return searchTermLiveData as LiveData<String>
    }

    fun getPlacesResults(): LiveData<List<Place>> {
        return placesResultsLiveData as LiveData<List<Place>>
    }

    fun getTripsResults(): LiveData<List<Trip>> {
        return tripsResultsLiveData as LiveData<List<Trip>>
    }

    fun getItemsResults(): LiveData<List<PackItem>> {
        return itemsResultsLiveData as LiveData<List<PackItem>>
    }

    fun getUsersResults(): LiveData<List<User>> {
        return usersResultsLiveData as LiveData<List<User>>
    }

    fun setSearchTerm(term: String, fragment: Int) {
        searchTerm = term
        searchTermLiveData.postValue(searchTerm)
        viewModelScope.launch {
            search(fragment)
        }

    }

    fun getSearchingStatus(): LiveData<Boolean> {
        return searchingStatusLive as LiveData<Boolean>
    }

    private suspend fun searchInPlaces() = withContext(dispatcher) {
        submitSearch()
        placesResults.clear()
        viewModelScope.launch {
            placesResultsLiveData.value = placesResults
        }
        viewModelScope.launch {

            placesRepository.getPlaces().observeForever { places ->
                CoroutineScope(dispatcher).launch {
                    if (places != null) {
                        for (place in places) {
                            val wordsTerm = processInput(searchTerm)
                            val wordsPlace =
                                (processInput(place.address) + processInput(place.name)).toMutableList()
                            place.overview?.let { wordsPlace += processInput(it) }
                            val distances = mutableListOf<Int>()
                            wordsTerm.map { word ->
                                wordsPlace.map { wordItem ->
                                    distances.add(
                                        calculateDamerauLevenshteinDistance(
                                            wordItem,
                                            word
                                        )
                                    )
                                }
                            }
                            val avgDistance = distances.minOrNull()
                            if ((avgDistance
                                    ?: 25) <= maxDamerauLevenshteinDistance && !placesResults.contains(
                                    place
                                )
                            ) {
                                placesResults.add(place)
                                Log.d("SearchViewModelsearchplacesdasdsd", "Place $place")
                            }
                        }
                    }
                    placesResultsLiveData.postValue(placesResults)
                    searchingStatusLive.postValue(false)
                }
            }
        }


    }

    private suspend fun searchInTrips() = withContext(dispatcher) {
        submitSearch()
        tripsResults.clear()
        viewModelScope.launch {
            tripsResultsLiveData.value = tripsResults
        }
        viewModelScope.launch {
            tripsRepository.getTrips().observeForever { trips ->

                CoroutineScope(dispatcher).launch {

                    if (trips != null) {
                        for (trip in trips) {
                            val wordsTerm = processInput(searchTerm)
                            val wordsTrip = processInput(trip.name)
                            val distances = mutableListOf<Int>()
                            wordsTerm.map { word ->
                                wordsTrip.map { wordTrip ->
                                    distances.add(
                                        calculateDamerauLevenshteinDistance(
                                            wordTrip,
                                            word
                                        )
                                    )
                                }
                            }
                            val avgDistance = distances.minOrNull()
                            if ((avgDistance
                                    ?: 25) <= maxDamerauLevenshteinDistance && !tripsResults.contains(
                                    trip
                                )
                            ) {
                                tripsResults.add(trip)
                            }
                        }
                    }
                    tripsResultsLiveData.postValue(tripsResults)
                    searchingStatusLive.postValue(false)
                }
            }


        }

    }


    private suspend fun searchInItems() = withContext(dispatcher) {
        submitSearch()
        itemsResults.clear()
        viewModelScope.launch {
            itemsResultsLiveData.value = itemsResults
        }
        val items = packItemRepository.getPackItems()
        for (item in items) {
            val wordsTerm = processInput(searchTerm)
            val wordsItem = processInput(item.itemName)
            val distances = mutableListOf<Int>()
            wordsTerm.map { word ->
                wordsItem.map { wordItem ->
                    distances.add(calculateDamerauLevenshteinDistance(wordItem, word))
                }
            }
            val avgDistance = distances.minOrNull()
            if ((avgDistance
                    ?: 25) <= maxDamerauLevenshteinDistance && !itemsResults.contains(item)
            ) {
                itemsResults.add(item)
            }
        }
        itemsResultsLiveData.postValue(itemsResults)
        searchingStatusLive.postValue(false)
    }


    private suspend fun calculateDamerauLevenshteinDistance(a: String, b: String): Int =
        withContext(Dispatchers.Default) {
            val trace = Firebase.performance.newTrace("Damerau_Lehvenstein_Distance_Calculation")
            trace.start()
            val dist: Array<IntArray> = Array(a.length + 1) { IntArray(b.length + 1) }
            var i = 0;
            var j = 0;
            var cost = 0

            while (i <= a.length) {
                dist[i][0] = i
                i++
            }
            while (j <= b.length) {
                dist[0][j] = j
                j++
            }

            i = 1
            while (i <= a.length) {
                j = 1
                while (j <= b.length) {
                    if (a[i - 1] == b[j - 1]) {
                        cost = 0
                    } else {
                        cost = 1
                    }
                    dist[i][j] = min(
                        dist[i - 1][j] + 1, min(
                            dist[i][j - 1] + 1,
                            dist[i - 1][j - 1] + cost
                        )
                    )
                    if (i > 1 && j > 1 && a[i - 1] == b[j - 2] && a[i - 2] == b[j - 1]) {
                        dist[i][j] = min(dist[i][j], dist[i - 2][j - 2] + 1)
                    }
                    j++
                }
                i++
            }
            trace.stop()
            return@withContext dist[a.length][b.length]
        }

    fun submitSearch(){
        SearchService.newInstance().submitSearch(Search(!itemsResults.isEmpty(),searchTerm,System.currentTimeMillis()))
    }

    private fun processInput(input: String): List<String> {
        // lowercase, remove punctuation, split into words
        return input.lowercase().trim().replace("/[.,\\/#!\$%\\^&\\*;:{}=\\-_`~()@]/g", ";")
            .split(";")
    }

    // singleton
    companion object {
        @Volatile
        private var instance: SearchViewModel? = null
        fun getInstance(
            tripsRepository: TripRepository,
            placesRepository: PlaceRepository,
            packItemRepository: PackItemRepository,
        ) =
            instance ?: synchronized(this) {
                instance ?: SearchViewModel(
                    tripsRepository,
                    placesRepository,
                    packItemRepository,
                ).also { instance = it }

            }
    }
}