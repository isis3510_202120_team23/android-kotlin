package com.example.trapp.ui.plan

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.plan.PlanMarioRepository

class PlansMarioViewModelFactory (private val planMarioRepository: PlanMarioRepository): ViewModelProvider.NewInstanceFactory() {


    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return PlansMarioViewModel(planMarioRepository) as T
    }

}
