package com.example.trapp.ui.plan

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.example.trapp.R
import com.example.trapp.data.model.Contact
import com.example.trapp.data.model.User
import com.example.trapp.data.model.UserGroup
import com.example.trapp.ui.user.UsersViewModel
import com.example.trapp.utilities.InjectorUtils
import com.google.android.material.radiobutton.MaterialRadioButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactsAdapter (private val contacts:List<Contact>,private val activity: AddPlanActivity,private val userGroup: UserGroup): RecyclerView.Adapter<ContactsAdapter.ViewHolder>(){

    inner class ViewHolder(contactView: View) : RecyclerView.ViewHolder(contactView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val contactButton: MaterialRadioButton = itemView.findViewById(R.id.radioButton)

        val context: Context = itemView.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val contactView = inflater.inflate(R.layout.contacts_fragment, parent, false)
        // Return a new holder instance
        return ViewHolder(contactView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val contact: Contact = contacts[position]
        // Set item views based on your views and data model
        val button = viewHolder.contactButton
        button.text=contact.name
        val userFactory = InjectorUtils.provideUsersViewModelFactory()
        val userViewModel =
            ViewModelProviders.of(activity, userFactory).get(UsersViewModel::class.java)

        button.setOnClickListener{

                    CoroutineScope(Dispatchers.IO).launch{
                        val cel= contact.mobileNumber?.replace(" ","")
                        if (cel != null) {
                            if(cel.startsWith("+57"))
                            {
                                cel.replace("+57","")
                            }

                        }
                        val agregar= cel?.let { it1 -> userViewModel.getAllUsersAsyncByPhone(it1) }
                       // Log.d("agregar", agregar.toString())
                        val userGroups= emptyList<String>().toMutableList()
                        if (agregar != null) {
                            if (agregar.firebaseId != "nullID") {
                                agregar.userGroups?.let { it1 -> userGroups.addAll(it1) }

                                userGroup.numberMembers = userGroup.numberMembers?.plus(1)
                                userGroups.add(userGroup.toString())
                                val updateUser = User(
                                    agregar.firebaseId,
                                    agregar.birthDate, agregar.dailyExpenses, agregar.email, "templateFirstName", "templateGender", "templateLastName",
                                    agregar.phone, userGroups
                                )
                                Log.d("Toca agregar", updateUser.toString())
                                userViewModel.updateUser(updateUser)

                                CoroutineScope(Dispatchers.Main).launch {
                                    button.isChecked=true
                                    Toast.makeText(
                                        viewHolder.context,
                                        "User " + contact.name + " successfully added to the plan.",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                            else{
                                CoroutineScope(Dispatchers.Main).launch {
                                    button.isChecked=false
                                    Toast.makeText(
                                        viewHolder.context,
                                        "User "+contact.name+" is not registered in TRAPP.",
                                        Toast.LENGTH_LONG
                                    ).show()}
                            }
                        }


                    }



        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return contacts.size
    }


}