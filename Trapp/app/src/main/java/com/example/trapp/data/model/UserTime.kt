package com.example.trapp.data.model

data class UserTime(
    val activity: String,
    val time: Double
)