package com.example.trapp.ui.budget

import androidx.lifecycle.ViewModel
import com.example.trapp.data.model.Budget
import com.example.trapp.data.place.BudgetRepository
import com.example.trapp.data.place.PlaceRepository

class BudgetViewModel (private val budgetRepository: BudgetRepository) : ViewModel(){

    suspend fun getBudgets()=budgetRepository.getBudgets()
    suspend fun insertBudget(budget: Budget)=budgetRepository.insertBudget(budget)

}
