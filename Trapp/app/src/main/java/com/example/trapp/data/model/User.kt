package com.example.trapp.data.model

data class User(
    var firebaseId: String? = null,
    val birthDate: String? = null,
    val dailyExpenses: List<DailyExpense>? = null,
    val email: String? = null,
    val firstName: String? = null,
    val gender: String? = null,
    val lastName: String? = null,
    val phone: String? = null,
    val userGroups: List<String>? = null
)


