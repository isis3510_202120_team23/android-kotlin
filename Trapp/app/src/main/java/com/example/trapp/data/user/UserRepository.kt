package com.example.trapp.data.user

import com.example.trapp.data.model.User

class UserRepository private constructor(private val UserDAO: UserDAO) {

    suspend fun getUsers() = UserDAO.getUsers()
    suspend fun getAllUsersAsync() = UserDAO.getAllusersAsync()
    suspend fun getAllUsersAsyncByPhone(tel:String) = UserDAO.getAllusersAsyncByPhone(tel)
    suspend fun getUserById(UserId: String) = UserDAO.getUserById(UserId)
    suspend fun createUser(User:User)= UserDAO.createUser(User)
    suspend fun updateUser(user: User)=UserDAO.updateUser(user)
    companion object {
        @Volatile
        private var instance: UserRepository? = null

        fun getInstance(UserDAO: UserDAO) =
            instance ?: synchronized(this) {
                instance ?: UserRepository(UserDAO).also { instance = it }
            }
    }


}