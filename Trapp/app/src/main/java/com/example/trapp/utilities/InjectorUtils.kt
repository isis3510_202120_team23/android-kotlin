package com.example.trapp.utilities

import android.content.Context
import com.example.trapp.data.*
import com.example.trapp.data.Contact.ContactRepository
import com.example.trapp.data.Contact.ContactViewModelFactory
import com.example.trapp.data.city.CityRepository
import com.example.trapp.data.packitem.PackItemRepository
import com.example.trapp.data.place.BudgetRepository
import com.example.trapp.data.place.DailyExpenseRepository
import com.example.trapp.data.place.PlaceRepository
import com.example.trapp.data.plan.PlanMarioRepository
import com.example.trapp.data.plan.PlanRepository
import com.example.trapp.data.trip.TripRepository
import com.example.trapp.data.user.UserRepository
import com.example.trapp.data.usergroup.UserGroupRepository
import com.example.trapp.ui.budget.BudgetsViewModelFactory
import com.example.trapp.ui.budget.DailyExpensesViewModelFactory
import com.example.trapp.ui.city.CitiesViewModelFactory
import com.example.trapp.ui.packitem.PackItemsViewModelFactory
import com.example.trapp.ui.place.PlacesViewModelFactory
import com.example.trapp.ui.trip.TripsViewModelFactory
import com.example.trapp.ui.plan.PlansMarioViewModelFactory
import com.example.trapp.ui.plan.PlansViewModelFactory
import com.example.trapp.ui.search.SearchViewModel
import com.example.trapp.ui.user.UsersViewModelFactory
import com.example.trapp.ui.usergroup.UserGroupsViewModelFactory

object InjectorUtils {

    fun provideTripsViewModelFactory(context: Context): TripsViewModelFactory {
        val tripRepository = TripRepository.getInstance(Database.getInstance().tripDao, AppDatabase.getInstance(context).tripDao())
        return TripsViewModelFactory(tripRepository)
    }
    fun providePlacesViewModelFactory(context: Context): PlacesViewModelFactory {
        val placeRepository = PlaceRepository.getInstance(Database.getInstance().placeDao, AppDatabase.getInstance(context).placeDao())
        return PlacesViewModelFactory(placeRepository)
    }
    fun provideBudgetsViewModelFactory(context: Context): BudgetsViewModelFactory {
        val budgetRepository = BudgetRepository.getInstance(AppDatabase.getInstance(context).budgetDao())
        return BudgetsViewModelFactory(budgetRepository)
    }
    fun provideDailyExpensesViewModelFactory(context: Context): DailyExpensesViewModelFactory {
        val dailyExpensesRepository =
            DailyExpenseRepository.getInstance(AppDatabase.getInstance(context).dailyExpenseDao())
        return DailyExpensesViewModelFactory(dailyExpensesRepository)
    }
    fun provideContactsViewModelFactory(context: Context): ContactViewModelFactory {
        val contactRepository = ContactRepository.getInstance( AppDatabase.getInstance(context).contactDao())
        return ContactViewModelFactory(contactRepository, context)
    }
    fun providePlansMarioViewModelFactory(): PlansMarioViewModelFactory {
        val planRepository = PlanMarioRepository.getInstance(Database.getInstance().planMarioDao)
        return PlansMarioViewModelFactory(planRepository)
    }
    fun providePlansViewModelFactory(): PlansViewModelFactory {
        val planRepository = PlanRepository.getInstance(Database.getInstance().planDao)
        return PlansViewModelFactory(planRepository)
    }
    fun provideUserGroupsViewModelFactory(): UserGroupsViewModelFactory {
        val userGroupRepository = UserGroupRepository.getInstance(Database.getInstance().userGroupDao)
        return UserGroupsViewModelFactory(userGroupRepository)
    }

    fun provideUsersViewModelFactory(): UsersViewModelFactory {
        val userRepository = UserRepository.getInstance(Database.getInstance().userDao)
        return UsersViewModelFactory(userRepository)
    }

    fun providePackItemsViewModelFactory(): PackItemsViewModelFactory {
        val packItemRepository = PackItemRepository.getInstance(Database.getInstance().packItemService)
        return PackItemsViewModelFactory(packItemRepository)
    }

    fun provideSearchViewModelSingleton(context:Context): SearchViewModel {
        val tripsRepository= TripRepository.getInstance(Database.getInstance().tripDao,AppDatabase.getInstance(context).tripDao())
        val placesRepository= PlaceRepository.getInstance(Database.getInstance().placeDao, AppDatabase.getInstance(context).placeDao())
        val packItemRepository= PackItemRepository.getInstance(Database.getInstance().packItemService)
        return SearchViewModel.getInstance(tripsRepository,placesRepository,packItemRepository)
    }

    fun provideCitiesViewModelFactory(context: Context): CitiesViewModelFactory {
        val cityRepository = CityRepository.getInstance(Database.getInstance().cityService, AppDatabase.getInstance(context).cityDao())
        return CitiesViewModelFactory(cityRepository)
    }
}