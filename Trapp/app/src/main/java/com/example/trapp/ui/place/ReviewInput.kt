package com.example.trapp.ui.place

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import com.example.trapp.R


class ReviewInput : AppCompatDialogFragment() {
    private var listener: ExampleDialogListener? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        val view: View = inflater.inflate(R.layout.layout_dialog_review, null)

      val editTextReview = view.findViewById<EditText>(R.id.edit_review)
        editTextReview.inputType =
            InputType.TYPE_CLASS_TEXT

        val editTextRating = view.findViewById<EditText>(R.id.edit_rating)
        editTextRating.inputType =
            InputType.TYPE_CLASS_NUMBER



        return AlertDialog.Builder(requireActivity())
            .setView(view)
            .setTitle("Write new review")
            .setNegativeButton("Cancel") {_, _, ->}
            .setPositiveButton("Ok") {_, _, ->
                val review = editTextReview.text.toString()
                val rating = editTextRating.text.toString()
                listener!!.applyTexts(review, rating)
            }
            .create()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = try {
            context as ExampleDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException(
                context.toString() +
                        "must implement ExampleDialogListener"
            )
        }
    }

    interface ExampleDialogListener {
        fun applyTexts(review: String?, rating: String?)
    }
}