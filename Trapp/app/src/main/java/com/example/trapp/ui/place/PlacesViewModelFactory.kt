package com.example.trapp.ui.place

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.place.PlaceRepository

class PlacesViewModelFactory (private val placeRepository: PlaceRepository): ViewModelProvider.NewInstanceFactory() {


    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return PlaceViewModel(placeRepository) as T
    }

}
