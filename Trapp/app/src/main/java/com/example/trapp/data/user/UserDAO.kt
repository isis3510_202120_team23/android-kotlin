package com.example.trapp.data.user


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.DailyExpense
import com.example.trapp.data.model.Place
import com.example.trapp.data.model.User
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class UserDAO {
    private val db = Firebase.firestore
    private val collectionName = "users"
    private val userList = mutableListOf<User>()
    private var user = User("nullId")

    private val users = MutableLiveData<List<User>>()
    private val userLive = MutableLiveData<User>()

    init {
        users.value = userList
        userLive.value = user
    }

    companion object {
        fun newInstance() = UserDAO()
    }

    suspend fun getAllusersAsync(): MutableList<User> = withContext(Dispatchers.IO) {
        return@withContext try {
            val data = db.collection(collectionName)
                .get()
                .await()
            Log.d("DATA",data.documents.toString())
            for (document in data.documents) {
                Log.d("doc",document.data.toString())
                userList.add(
                    User(
                        document.id,
                        document.data!!.getValue("birthday") as String,
                        document.data!!.getValue("dailyExpenses") as List<DailyExpense>,
                        document.data!!.getValue("email") as String,
                        document.data!!.getValue("firstName") as String,
                        document.data!!.getValue("gender") as String,
                        document.data!!.getValue("lastName") as String,
                        document.data!!.getValue("phone") as String,
                        document.data!!.getValue("userGroups") as List<String>
                    )
                )
            }
            Log.d("UserListDAO",userList.toString())
            userList
        } catch (e: Exception) {
            userList
        }
    }
    suspend fun updateUser(user: User) = withContext(Dispatchers.IO) {
        return@withContext try {
            val data = db.collection(collectionName).whereEqualTo("phone",user.phone).get().await()
            Log.d("DATA",data.documents.toString())

            for (document in data.documents) {
                Log.d("doc",document.data.toString())
                db.collection(collectionName).document(document.id).set(user)
            }
        } catch (e: Exception) {
            Log.d("UserUpdateCatch",e.stackTraceToString())
        }
    }
    suspend fun getAllusersAsyncByPhone(tel:String): User = withContext(Dispatchers.IO) {
        return@withContext try {
            val tels = mutableListOf<String>()
            tels.add(tel)
            tels.add("+57"+tel)
            val data = db.collection(collectionName).whereIn("phone", tels)
                .get()
                .await()
            var userResp = User("nullID")
            Log.d("DATA",data.documents.toString())
            for (document in data.documents) {
                Log.d("doc",document.data.toString())
                if(document.data!!.containsKey("birthDate")) {
                    if(document.data!!.getValue("birthDate")!=null) {
                        userResp = User(
                            document.id,
                            document.data!!.getValue("birthDate") as String,
                            document.data!!.getValue("dailyExpenses") as List<DailyExpense>,
                            document.data!!.getValue("email") as String,
                            document.data!!.getValue("firstName") as String,
                             document.data!!.getValue("gender") as String,
                             document.data!!.getValue("lastName") as String,
                            document.data!!.getValue("phone") as String,
                            document.data!!.getValue("userGroups") as List<String>
                        )
                    }
                }
            }
            Log.d("UserListDAO",userResp.toString())
            userResp
        } catch (e: Exception) {
            Log.d("UserListDAOCatch",e.stackTraceToString())
            null
        }!!
    }

    suspend fun getuserByIdAsync(userId: String): User = withContext(Dispatchers.IO)
    {
        return@withContext try {
            val document = db.collection(collectionName).document(userId)
                .get().await()
           // println("-----------------document-------" + (document.data?.getValue("dailyExpenses")?.javaClass
             //   ?: null))

            val list = mutableListOf<DailyExpense>()

          //  for(daily in document.data!!.getValue("dailyExpenses")
           // {
           //     list.add(DailyExpense(daily.value))
           // }

            user = document.toObject(User::class.java)!!
            user.firebaseId = document.id

            /*ser(
                document.id,
                document.data!!.getValue("birthDate") as String,
                document.data!!.getValue("dailyExpenses") as List<DailyExpense>,
                document.data!!.getValue("email") as String,
                document.data!!.getValue("firstName") as String,
                document.data!!.getValue("gender") as String,
                document.data!!.getValue("lastName") as String,
                document.data!!.getValue("phone") as String,
                document.data!!.getValue("userGroups") as List<String>
            )*/
            user
        } catch (e: Exception) {
            println("excepcion ----------------" + e)
            user
        }
    }

    suspend fun getUsers(): LiveData<List<User>> {
        getAllusersAsync()
        return users
    }

    suspend fun getUserById(userId: String): LiveData<User> {
        getuserByIdAsync(userId)
        println("-------------------------------el mega user" + user)
        userLive.value = user
        return userLive
    }
    suspend fun createUser(user:User){
        db.collection(collectionName).add(user)
    }
}
