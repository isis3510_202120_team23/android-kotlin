package com.example.trapp.ui.budget

import androidx.lifecycle.ViewModel
import com.example.trapp.data.model.DailyExpense
import com.example.trapp.data.place.BudgetRepository
import com.example.trapp.data.place.DailyExpenseRepository
import com.example.trapp.data.place.PlaceRepository

class DailyExpenseViewModel (private val dailyExpenseRepository: DailyExpenseRepository) : ViewModel(){

    suspend fun getDailyExpensesByBudgetId(budgetId: Long)=dailyExpenseRepository.getDailyExpensesByBudgetId(budgetId)
    suspend fun insertDailyExpense(dailyExpense: DailyExpense)=dailyExpenseRepository.insertDailyExpense(dailyExpense)
}
