package com.example.trapp.ui.packitem

import android.content.res.ColorStateList
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.trapp.R
import com.example.trapp.data.model.PackItem
import com.google.android.material.color.MaterialColors.getColor
import soup.neumorphism.NeumorphCardView
import android.util.Pair


class PackItemsAdapter(private val items: ArrayList<Pair<Boolean, PackItem>>) :
    RecyclerView.Adapter<PackItemsAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val toggleButton: SwitchCompat = itemView.findViewById<SwitchCompat>(R.id.pack_item_switch)
        val cardWrapper: NeumorphCardView =
            itemView.findViewById<NeumorphCardView>(R.id.toggle_neumorph_wrapper)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout

        val itemView = inflater.inflate(R.layout.pack_item_row, parent, false)
        // Return a new holder instance
        return ViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, index: Int) {
        // Get the data model based on position?
        val packItem: Pair<Boolean, PackItem> = items[index]
        // Set item views based on your views and data model
        val toggle = viewHolder.toggleButton
        val card = viewHolder.cardWrapper
        val itemName: String = packItem.second.itemName
        //val itemId: String? = savedInstanceState.getString("packItemId")
        toggle.text = itemName
        val thumbStates:ColorStateList?

        var chosenButtonColor: Int
        var chosenBackGroundColor: Int

        val selectedColor:Int
        val notSelectedColor:Int

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
            selectedColor= toggle.context.getColor(R.color.orange)
            notSelectedColor=toggle.context.getColor(R.color.primary)

        }
        else{
            selectedColor= getColor(toggle.context, R.attr.colorSecondary, 0)
            notSelectedColor=getColor(toggle.context, R.attr.colorPrimary, 0)
        }

        chosenBackGroundColor = notSelectedColor
        chosenButtonColor = selectedColor

        if (packItem.first) {
            // suggested
            chosenBackGroundColor = selectedColor
            chosenButtonColor = notSelectedColor
        }
        thumbStates = ColorStateList(
            arrayOf(
                intArrayOf(-android.R.attr.state_enabled),
                intArrayOf(android.R.attr.state_checked),
                intArrayOf()
            ), intArrayOf(
                chosenButtonColor,
                chosenButtonColor,
                chosenButtonColor
            )
        )

        card.setBackgroundColor(chosenBackGroundColor)
        toggle.thumbDrawable.setTintList(thumbStates)
        toggle.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                //TODO addToTripItemList()
                Log.d("Toggle", "Toggle $itemName ON")
            } else {
                //TODO removeFromTripItemList()
                Log.d("Toggle", "Toggle $itemName OFF")
            }
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return items.size
    }

}