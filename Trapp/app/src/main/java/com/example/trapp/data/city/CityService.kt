package com.example.trapp.data.city

import android.util.ArrayMap
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.City
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class CityService {

    private val db = Firebase.firestore
    private val citiesCollection = "cities"

    private val cityList = ArrayMap<String,City>()

    private val cities = MutableLiveData<ArrayMap<String,City>>()

    init{
        cities.value=cityList
        CoroutineScope(Dispatchers.IO).launch {
            getAllCitiesAsync()
        }
    }

    companion object {
        fun newInstance() = CityService()
    }

    suspend fun getAll():List<City> {
        return withContext(Dispatchers.IO) {
            try {
                val cities = ArrayList<City>()
                val citiesRef = db.collection(citiesCollection)
                val query = citiesRef.get().await()
                for (cityDoc in query) {
                    val city = City(
                        cityDoc.id,
                        cityDoc.data["latitude"] as Double,
                        cityDoc.data["longitude"] as Double,
                        cityDoc.data["name"] as String
                    )
                    cities.add(city)
                }
                cities as List<City>
            } catch (e: Exception) {
                emptyList<City>()
            }
        }
    }

    suspend fun getCity(id:String):City? {
        return withContext(Dispatchers.IO) {
            try {
                if(!cityList.containsKey(id)) {
                    val cityDoc = db.collection(citiesCollection).document(id).get().await()
                    cityList[id] = City(
                        cityDoc.id,
                        cityDoc.data?.get("latitude") as Double,
                        cityDoc.data?.get("longitude") as Double,
                        cityDoc.data?.get("name") as String
                    )
                }
                cityList[id]
            } catch (e: Exception) {
                null
            }
        }
    }


    private suspend fun getAllCitiesAsync(){
        withContext(Dispatchers.IO) {
        try {
            val data = db.collection(this@CityService.citiesCollection)
                .get()
                .await()
            for (document in data.documents) {

                cityList[document.id] = City(
                    document.id,
                    document.data?.get("latitude") as Double,
                    document.data?.get("longitude") as Double,
                    document.data?.get("name") as String
                )
            }
            cities
        } catch (e: Exception) {
            cities
        }
    }
    }


    suspend fun getSuggestedItemsIdsCityAsync(cityId: String): MutableList<String> =
        withContext(Dispatchers.IO)
        {
            val items: MutableList<String> = mutableListOf<String>()
            return@withContext try {
                val city = db.collection(citiesCollection).document(cityId)
                    .get()
                    .await()
                val references=city.data?.getValue("items") as ArrayList<DocumentReference>
                    for (reference in references) {
                        items.add(reference.id)
                    }
                items
            } catch (e: Exception) {
                Log.d("CityService", "getSuggestedItemsIdsCityAsync: ${e.stackTraceToString()}")
                items
            }
        }

}