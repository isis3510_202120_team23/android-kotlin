package com.example.trapp.data.plan

import com.example.trapp.data.model.City
import com.example.trapp.data.model.Plan

class PlanRepository private constructor(private val planDAO: PlanDAO) {

    suspend fun getPlans() = planDAO.getPlans()
    suspend fun createPlan(plan:Plan)=planDAO.createPlan(plan)
    companion object {
        @Volatile
        private var instance: PlanRepository? = null

        fun getInstance(planDAO: PlanDAO) =
            instance ?: synchronized(this) {
                instance ?: PlanRepository(planDAO).also { instance = it }
            }
    }

}