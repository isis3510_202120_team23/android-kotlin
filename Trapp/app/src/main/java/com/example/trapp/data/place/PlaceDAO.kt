package com.example.trapp.data.place


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.Place
import com.example.trapp.data.model.PlaceReview
import com.example.trapp.data.model.Review
import com.example.trapp.data.model.User
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import com.google.firebase.perf.ktx.performance
import kotlinx.coroutines.*


class PlaceDAO {
    private val db = Firebase.firestore
    private val collectionName = "places"
    private val placeList = mutableListOf<Place>()
    private var place = Place("nullId", "", "", 0.0, 0.0, "", null, null, null)

    private val places = MutableLiveData<List<Place>>()
    private val placeLive = MutableLiveData<Place>()

    init {
        places.value = placeList
        placeLive.value = place
    }

    companion object {
        fun newInstance() = PlaceDAO()
    }


    suspend fun getAllPlacesAsync() {
        withContext(Dispatchers.IO) {
            try {
                val trace = Firebase.performance.newTrace("get All Places Async")
                trace.start()
                val data = db.collection(collectionName)
                    .get()
                    .await()

                for (document in data.documents) {

                    placeList.add(
                        Place(
                            document.id,
                            document.data?.getValue("address") as String,
                            document.data!!.getValue("img") as String,
                            document.data!!.getValue("latitude") as Double,
                            document.data!!.getValue("longitude") as Double,
                            document.data!!.getValue("name") as String,
                            null,
                            null,
                            document.data!!.getValue("overview") as String
                        )
                    )

                }
                trace.stop()
            } catch (e: Exception) {
                Log.d("placedao", e.stackTraceToString())
            }

        }
    }

    suspend fun getPlaceByIdAsync(placeId: String){
        withContext(Dispatchers.IO){
            try {
                val document = db.collection(collectionName).document(placeId)
                    .get()
                    .await()


                    val reviews = document.data!!.getValue("reviews") as ArrayList<HashMap<String,*>>
                Log.d("TIPOJO",reviews[0].javaClass.toString())

                val listica = mutableListOf<PlaceReview>()

                    reviews.forEach({ review ->
                        val placeReview = PlaceReview(review["calification"] as Long, review["comment"] as String, review["author"] as String)
                        listica.add(placeReview)
                    })
                    place = Place(
                        document.id,
                        document.data?.getValue("address") as String,
                        document.data!!.getValue("img") as String,
                        document.data!!.getValue("latitude") as Double,
                        document.data!!.getValue("longitude") as Double,
                        document.data!!.getValue("name") as String,
                        null,
                        listica,
                        document.data!!.getValue("overview") as String
                    )
                    Log.d("DOCUMENTACIONAO", place.toString())
                    place.firebaseId = document.id

                    place


            } catch (e: Exception) {
                Log.d("placedao", e.stackTraceToString())
                place
            }
        }
    }

    suspend fun updateReviewsAsync(review: PlaceReview, placeId: String)
    {
        withContext(Dispatchers.IO){

            try {
                val document = db.collection(collectionName).document(placeId)
                    .get()
                    .await()


                val review2 = hashMapOf("author" to review.author, "comment" to review.comment, "calification" to review.calification)
                val place = placeLive.value

                val reviews = document.data!!.getValue("reviews") as ArrayList<HashMap<String,*>>
                    reviews.add(review2)
                if (place != null) {
                    val riviu = place.reviews as MutableList<PlaceReview>
                    riviu.add(review)
                    place.reviews = riviu as List<PlaceReview>
                    placeLive.postValue(place!!)
                }

                db.collection(collectionName).document(placeId)
                    .update("reviews", reviews)
            } catch (e: Exception) {
                Log.d("placedao", e.stackTraceToString())
                place
            }
        }
    }

    suspend fun getPlaces(): LiveData<List<Place>> {
        getAllPlacesAsync()
        return places
    }
    suspend fun getPlaceById(placeId: String): LiveData<Place> {

                getPlaceByIdAsync(placeId)

                placeLive.value = place


        return placeLive
    }

    suspend fun updateReviews(review: PlaceReview, placeId: String){
        updateReviewsAsync(review, placeId)
    }


}
