package com.example.trapp.data

import com.example.trapp.data.city.CityService
import com.example.trapp.data.packitem.PackItemService
import com.example.trapp.data.place.PlaceDAO
import com.example.trapp.data.plan.PlanDAO
import com.example.trapp.data.plan.PlanMarioDAO
import com.example.trapp.data.trip.TripDAO
import com.example.trapp.data.user.UserDAO
import com.example.trapp.data.usergroup.UserGroupDAO

class Database private constructor() {

    //    var activityDao = ActivityDAO()
//        private set
    var cityService = CityService()
//        private set
    var packItemService = PackItemService()
        private set
    var placeDao = PlaceDAO()
        private set

    var planMarioDao = PlanMarioDAO()
        private set
    var planDao = PlanDAO()
        private set
//    var reviewDao = ReviewDAO()
//        private set
    var tripDao = TripDAO()
        private set
//    var userTimeDao = UserTimeDAO()
//        private set

    var userGroupDao = UserGroupDAO()
        private set

    var userDao = UserDAO()
        private set

    companion object {
        @Volatile
        private var instance: Database? = null
        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: Database().also { instance = it }
            }
    }
}