package com.example.trapp.data.model

data class PackItem(
        val itemId: String,
        val itemName: String
)