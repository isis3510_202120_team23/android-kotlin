package com.example.trapp.data.Contact

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.place.PlaceRepository
import com.example.trapp.ui.place.PlaceViewModel

class ContactViewModelFactory (private val contactRepository: ContactRepository, private val context: Context): ViewModelProvider.NewInstanceFactory() {


    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return ContactViewModel(contactRepository,context) as T
    }

}