package com.example.trapp.data.model

data class PlanMario(
    var firebaseId:String? = null,
    val active: Boolean? = null,
    val activities: List<Int>? = null,
    val cities: List<String>? = null,
    //Debería ser expenses en vez de spendings porque esa palabra no existe xd
    val dailyExpenses: List<Double>? = null,
    val endDate: String? = null,
    val items: List<String>? = null,
    val name: String? = null,
    val price: Double? = null,
    val reviews: List<ReviewMario>? = null,
    val startDate: String? = null,
    val trip: Int? = null
)