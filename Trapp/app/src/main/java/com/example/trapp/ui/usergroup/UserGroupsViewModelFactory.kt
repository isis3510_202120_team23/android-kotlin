package com.example.trapp.ui.usergroup

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.usergroup.UserGroupRepository

class UserGroupsViewModelFactory (private val UserGroupRepository: UserGroupRepository): ViewModelProvider.NewInstanceFactory() {


    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return UserGroupsViewModel(UserGroupRepository) as T
    }

}
