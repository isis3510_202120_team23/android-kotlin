package com.example.trapp.ui.search

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import com.example.trapp.R
import com.example.trapp.ui.fragments.navbar.NavbarFragment
import com.example.trapp.utilities.InjectorUtils
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import soup.neumorphism.NeumorphCardView
import soup.neumorphism.NeumorphImageButton
import soup.neumorphism.ShapeType
import soup.neumorphism.NeumorphShapeAppearanceModel
import kotlin.coroutines.coroutineContext


// constantes para saber donde buscar

const val PLACESPOSITION = 0
const val TRIPSPOSITION = 1
const val ITEMSPOSITION = 2
const val PLACESLABEL = "Places"
const val TRIPSLABEL = "Trips"
const val ITEMSLABEL = "Items"

class SearchActivity : AppCompatActivity() {


    private lateinit var searchInput: EditText
    private lateinit var searchButton: NeumorphImageButton

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager2

    private lateinit var viewPagerCallback: ViewPager2.OnPageChangeCallback

    private lateinit var viewModel: SearchViewModel

    private lateinit var loadingBar: ProgressBar

    fun setView() {

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        loadingBar = findViewById(R.id.toploadingbar)
        loadingBar.visibility = View.VISIBLE
        viewModel = InjectorUtils.provideSearchViewModelSingleton(this)

        searchInput = findViewById<EditText>(R.id.SearchBarInput)
        searchButton = findViewById<NeumorphImageButton>(R.id.searchButton)

        setupSearchingStatus()
        setupSearchButton()
        setupSearchInput()
        setupViewPager()
        setupNavbar(savedInstanceState)
    }

    private fun setupViewPager() {
        tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        tabLayout.setSelectedTabIndicator(null)

        tabLayout.setPadding(0, 0, 0, 0)

        val adapter = SearchViewPagerAdapter(this)
        viewPager = findViewById<ViewPager2>(R.id.viewPager)
        viewPager.adapter = adapter

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->

            tab.setCustomView(R.layout.viewpager_tab_normal)
            when (position) {
                PLACESPOSITION -> {
                    tab.text = PLACESLABEL
                }
                TRIPSPOSITION -> {
                    tab.text = TRIPSLABEL
                }
                ITEMSPOSITION -> {
                    tab.text = ITEMSLABEL
                }
                else -> {
                    "ERROR"
                }
            }
        }.attach()
        viewPager.setCurrentItem(TRIPSPOSITION, false)
        tabLayout.getTabAt(TRIPSPOSITION)?.customView?.let { setTabViewAsSelected(it, true) }
        viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL


        viewPagerCallback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                val tabs = tabLayout.tabCount
                for (i in 0 until tabs) {
                    val tabView = tabLayout.getTabAt(i)?.customView
                    lifecycleScope.launch {
                        viewModel.search(i)
                    }
                    if (tabView != null) {

                        if (i == position) {
                            setTabViewAsSelected(tabView, true)

                        } else {
                            setTabViewAsSelected(tabView, false)
                        }
                    }
                }
            }
        }

        viewPager.registerOnPageChangeCallback(viewPagerCallback)
    }

    private fun setTabViewAsSelected(tabView: View, selected: Boolean) {
        val itemView = tabView.findViewById<NeumorphCardView>(R.id.tabCard)
        val textView = tabView.findViewById<TextView>(android.R.id.text1)
        if (selected) {
            itemView.setShapeType(ShapeType.FLAT)
            itemView.setBackgroundColor(getColor(R.color.orange))
            textView.setTextColor(getColor(R.color.black))
        } else {
            itemView.setShapeType(ShapeType.DEFAULT)
            itemView.setBackgroundColor(getColor(R.color.primary))
            textView.setTextColor(getColor(R.color.white))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewPager.unregisterOnPageChangeCallback(viewPagerCallback)
    }

    private fun setupNavbar(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                val fragment = NavbarFragment()
                replace(R.id.navbar_fragment, fragment, "NavbarFragment")
            }
        }
    }

    private fun setupSearchButton() {
        searchButton.setOnClickListener {
            val searchText = searchInput.text.toString()
            viewModel.setSearchTerm(searchText, tabLayout.selectedTabPosition)
        }
    }

    private fun setupSearchingStatus(){
        viewModel.getSearchingStatus().observe(this){ status->
            if (status) {
                loadingBar.visibility = View.VISIBLE
            } else {
                loadingBar.visibility = View.GONE
            }
        }

    }


    private fun setupSearchInput() {
        searchInput.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    viewModel.setSearchTerm(
                        searchInput.text.toString(),
                        tabLayout.selectedTabPosition
                    )
                    true
                }
                else -> false
            }
        }
    }

}