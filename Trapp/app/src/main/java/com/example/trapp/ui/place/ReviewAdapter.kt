package com.example.trapp.ui.place

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trapp.R
import com.example.trapp.data.model.Place
import com.example.trapp.data.model.PlaceReview
import com.example.trapp.data.model.Review
import com.example.trapp.ui.packitem.PackItemActivity
import com.example.trapp.ui.plan.PlanActivity
import java.util.*

class ReviewAdapter (private val reviews:List<PlaceReview>): RecyclerView.Adapter<ReviewAdapter.ViewHolder>(){

    inner class ViewHolder(reviewView: View) : RecyclerView.ViewHolder(reviewView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row

        val reviewAuthor: TextView = itemView.findViewById(R.id.textAuthor)
        val reviewContent: TextView = itemView.findViewById(R.id.textReview)
        val reviewRating: TextView = itemView.findViewById(R.id.textRating)
        val context: Context = itemView.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val reviewView = inflater.inflate(R.layout.review_card, parent, false)
        // Return a new holder instance
        return ViewHolder(reviewView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val review: PlaceReview = reviews[position]
        // Set item views based on your views and data model
        val author = viewHolder.reviewAuthor
        val content = viewHolder.reviewContent
        val rating = viewHolder.reviewRating
        author.setText(review.author)
        content.setText(review.comment)
        rating.setText("Rating: " + review.calification.toString())
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return reviews.size
    }



}