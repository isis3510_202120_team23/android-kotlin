package com.example.trapp.data.packitem

import com.example.trapp.data.model.PackItem

class PackItemRepository private constructor(private val packItemService: PackItemService)  {

    private suspend fun getPackItemsFromRemote(): List<PackItem> {
        return packItemService.getAll()
    }

    // solo online
    suspend fun getPackItems() : List<PackItem> {
        return getPackItemsFromRemote()
    }

    // solo online
    suspend fun getSuggestedTripPackItems(tripId:String) = packItemService.getSuggestedTripPackItems(tripId)

    // solo online
    suspend fun getSuggestedCityPackItems(cityId:String) = packItemService.getSuggestedCityPackItems(cityId)


    companion object {
        @Volatile
        private var instance: PackItemRepository? = null

        fun getInstance(packItemService: PackItemService) =
            instance ?: synchronized(this) {
                instance ?: PackItemRepository(packItemService).also { instance = it }
            }
    }

}
