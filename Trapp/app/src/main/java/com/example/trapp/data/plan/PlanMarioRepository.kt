package com.example.trapp.data.plan

class PlanMarioRepository private constructor(private val planMarioDAO: PlanMarioDAO) {

    suspend fun getPlans() = planMarioDAO.getPlans()
    suspend fun getPlanById(planId: String) = planMarioDAO.getPlanById(planId)

    companion object {
        @Volatile
        private var instance: PlanMarioRepository? = null

        fun getInstance(planDAO: PlanMarioDAO) =
            instance ?: synchronized(this) {
                instance ?: PlanMarioRepository(planDAO).also { instance = it }
            }
    }


}