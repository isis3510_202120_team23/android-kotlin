package com.example.trapp.ui.usergroup

import androidx.lifecycle.ViewModel
import com.example.trapp.data.usergroup.UserGroupRepository

class UserGroupsViewModel (private val UserGroupRepository: UserGroupRepository) : ViewModel(){

    suspend fun getUserGroups()=UserGroupRepository.getUserGroups()
    suspend fun getUserGroupById(UserGroupId: String)=UserGroupRepository.getUserGroupById(UserGroupId)

}
