package com.example.trapp.data.plan


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.PlanMario
import com.example.trapp.data.model.ReviewMario
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class PlanMarioDAO {
    private val db = Firebase.firestore
    private val collectionName = "plansMario"
    private val planList = mutableListOf<PlanMario>()
    private var plan = PlanMario()

    private val plans = MutableLiveData<List<PlanMario>>()
    private val planLive = MutableLiveData<PlanMario>()

    init {
        plans.value = planList
        planLive.value = plan
    }

    companion object {
        fun newInstance() = PlanMarioDAO()
    }

    suspend fun getAllPlansAsync(): MutableList<PlanMario> = withContext(Dispatchers.IO) {
        return@withContext try {
            val data = db.collection(collectionName)
                .get()
                .await()

            for (document in data.documents) {
               var plan = document.toObject(PlanMario::class.java)!!
                plan.firebaseId = document.id
                planList.add(plan)
            }
            planList
        } catch (e: Exception) {
            planList
        }
    }

    suspend fun getPlanByIdAsync(planId: String): PlanMario = withContext(Dispatchers.IO)
    {
        return@withContext try {
            val document = db.collection(collectionName).document(planId)
                .get().await()

            plan = PlanMario(
                document.id,
                document.data!!.getValue("active") as Boolean,
                document.data!!.getValue("activities") as List<Int>,
                   document.data!!.getValue("cities") as List<String>,
                  document.data!!.getValue("dailyExpenses") as List<Double>,
                  document.data!!.getValue("endDate") as String,
                  document.data!!.getValue("items") as List<String>,
                  document.data!!.getValue("name") as String,
                 document.data!!.getValue("price") as Double,
                 document.data!!.getValue("reviews") as List<ReviewMario>,
                 document.data!!.getValue("startDate") as String,
            )
            plan
        } catch (e: Exception) {
            println("-----------------catch----------------------")
            plan
        }
    }

    suspend fun getPlans(): LiveData<List<PlanMario>> {
        getAllPlansAsync()
        return plans
    }

    suspend fun getPlanById(planId: String): LiveData<PlanMario> {
        getPlanByIdAsync(planId)

        println("-------------------------------" + planLive.value)
        planLive.value = plan
        return planLive
    }

}
