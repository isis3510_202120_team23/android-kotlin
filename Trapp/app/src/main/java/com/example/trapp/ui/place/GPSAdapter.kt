package com.example.trapp.ui.place

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.trapp.R
import com.example.trapp.data.model.Place
import com.example.trapp.databinding.TripCardBinding
import com.example.trapp.ui.packitem.PackItemActivity
import com.example.trapp.ui.plan.PlanActivity
import java.util.*

class GPSAdapter (private val trips:List<Place>): RecyclerView.Adapter<GPSAdapter.ViewHolder>(){

    inner class ViewHolder(tripBinding: TripCardBinding) : RecyclerView.ViewHolder(tripBinding.root) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val planButton: AppCompatButton =   tripBinding.planButton
        val planImage: ImageView=   tripBinding.planImagePreview
        val planPrice: TextView=    tripBinding.planPrice
        val context: Context =  tripBinding.root.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val tripView = TripCardBinding.inflate(LayoutInflater.from(parent.context),
            parent, false)
        return ViewHolder(tripView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val place: Place = trips[position]
        // Set item views based on your views and data model
        val button = viewHolder.planButton
        val image = viewHolder.planImage
        val price = viewHolder.planPrice

        val imgUrl: String = place.img
        Glide.with(image).load(imgUrl).placeholder(R.mipmap.city3_foreground).into(image)
            price.text= place.address

        button.setOnClickListener{
            val bundle: Bundle = bundleOf("place" to place.firebaseId)
            val intent = Intent(
                button.context,
                PlaceActivity::class.java
            )
            intent.putExtra("id", place.firebaseId)
            intent.putExtra("name", place.name)
            intent.putExtra("img", place.img)
            intent.putExtra("overview", place.overview)
            ContextCompat.startActivity(button.context, intent, bundle)
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return trips.size
    }



}