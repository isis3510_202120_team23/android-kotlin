package com.example.trapp.data.place

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.budget.BudgetRoomDAO
import com.example.trapp.data.model.Budget
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class BudgetRepository private constructor(private val budgetRoomDAO: BudgetRoomDAO) {

    suspend fun getBudgets(): LiveData<List<Budget>> {

        val budgetsRoomLive = MutableLiveData<List<Budget>>()

        val budgetsRoom = withContext(Dispatchers.IO) { budgetRoomDAO.getAll() }


        if (budgetsRoom.isNotEmpty()) {
            if (Calendar.getInstance().timeInMillis - budgetsRoom[0].saveDate < 6.048e+8) {
                budgetsRoomLive.value = budgetsRoom
            } else {
                withContext(Dispatchers.IO) {
                    budgetsRoom.forEach { budget ->
                        budgetRoomDAO.delete(budget)
                    }
                }
            }
        }

        return budgetsRoomLive

    }

    suspend fun insertBudget(budget: Budget) {
        withContext(Dispatchers.IO) { budgetRoomDAO.insert(budget) }
    }

    companion object {
        @Volatile
        private var instance: BudgetRepository? = null

        fun getInstance(budgetRoomDAO: BudgetRoomDAO) =
            instance ?: synchronized(this) {
                instance ?: BudgetRepository(budgetRoomDAO).also { instance = it }
            }
    }


}