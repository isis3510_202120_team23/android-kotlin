package com.example.trapp.ui.user

import androidx.lifecycle.ViewModel
import com.example.trapp.data.model.User
import com.example.trapp.data.user.UserRepository

class UsersViewModel (private val UserRepository: UserRepository) : ViewModel(){

    suspend fun getUsers()=UserRepository.getUsers()
    suspend fun getAllUsersAsync()=UserRepository.getAllUsersAsync()
    suspend fun getAllUsersAsyncByPhone(tel: String?)= tel?.let {
        UserRepository.getAllUsersAsyncByPhone(
            it
        )
    }
    suspend fun getUserById(UserId: String)=UserRepository.getUserById(UserId)
    suspend fun createUser(user: User)=UserRepository.createUser(user)
    suspend fun updateUser(user:User)=UserRepository.updateUser(user)

}
