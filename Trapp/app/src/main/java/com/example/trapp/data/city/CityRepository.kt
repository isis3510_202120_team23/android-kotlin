package com.example.trapp.data.city

import android.content.Context
import android.util.ArrayMap
import com.example.trapp.data.model.City
import com.example.trapp.utilities.Connectivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class CityRepository private constructor(
    private val cityService: CityService,
    private val cityDao: CityDAO
) {

    private val LONGEVITYDAYS: Long = 365

    private suspend fun getCitieslocal(): List<City> =
        withContext(Dispatchers.IO) {
            return@withContext cityDao.getAll()
        }

    private suspend fun getCitiesFromServer(): List<City> {
        // ya usa otro hilo en el service
        return cityService.getAll()
    }

    private suspend fun updateCity(id: String): City? =
        withContext(Dispatchers.IO) {
            val newCity = cityService.getCity(id)
            if (newCity != null) {
                cityDao.update(newCity)
            }
            return@withContext newCity
        }

    private fun insertCity(city: City) {
        CoroutineScope(Dispatchers.IO).launch {
            cityDao.insert(city)
        }
    }

    private fun insertCities(cities: List<City>) {
        CoroutineScope(Dispatchers.IO).launch {
            val calendar = Calendar.getInstance()
            cities.forEach { it ->
                it.timestamp = calendar.timeInMillis
                cityDao.insert(it)
            }
        }
    }

    fun getCities(context: Context): ArrayMap<String, City> {

        val connectivity = Connectivity.getInstance()

        /*
        *   1. revisar room
        *   2. revisar vigencia (ciudades = 1 año)
        *   3. revisar conexión a internet
        *   4. si room vacio o expirada, fetch de internet
        *   5. si no hay internet, retornar room
        * */

        val cityList: ArrayMap<String, City> = ArrayMap<String, City>()

        CoroutineScope(Dispatchers.IO).launch {
            val citieslocal = getCitieslocal()
            if (citieslocal.isEmpty()) {
                if (connectivity.getInternet(context)) {
                    val citiesFromServer = getCitiesFromServer()
                    cityList.putAll(citiesFromServer.associateBy { it.id })
                    insertCities(citiesFromServer)
                }
            } else {
                val calendar = Calendar.getInstance()
                for (city in citieslocal) {
                    val timeElapsed = calendar.timeInMillis - city.timestamp!!
                    val timeElapsedDays:Long= (timeElapsed / (1000 * 60 * 60 * 24))
                    if ((timeElapsedDays >= LONGEVITYDAYS) && connectivity.getInternet(
                            context
                        )
                    ) {
                        val updatedCity: City? = updateCity(city.id)
                        updatedCity?.let {
                            cityList[it.id] = it
                        }
                    } else {
                        cityList[city.id] = city
                    }
                }
            }
        }

        return cityList
    }

    companion object {
        @Volatile
        private var instance: CityRepository? = null

        fun getInstance(cityService: CityService, cityDao: CityDAO) =
            instance ?: synchronized(this) {
                instance ?: CityRepository(cityService, cityDao).also { instance = it }
            }
    }

}