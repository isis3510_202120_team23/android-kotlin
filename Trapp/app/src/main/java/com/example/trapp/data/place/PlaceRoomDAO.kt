package com.example.trapp.data.place

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.trapp.data.model.Place


@Dao
interface PlaceRoomDAO {
    @Query("SELECT * FROM place")
    fun getAll(): List<Place>

    @Query("SELECT * FROM place WHERE firebaseId =:id")
    fun getById(id: String): Place

    @Insert
    fun insertAll(vararg place: Place)

    @Delete
    fun delete(place: Place)

    @Query("DELETE FROM place")
    fun deleteAll()

}