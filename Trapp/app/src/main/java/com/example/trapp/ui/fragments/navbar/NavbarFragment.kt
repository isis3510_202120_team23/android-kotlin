package com.example.trapp.ui.fragments.navbar

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.trapp.R
import com.example.trapp.databinding.NavbarBinding
import com.example.trapp.ui.budget.BudgetActivity
import com.example.trapp.ui.login.*
import com.example.trapp.ui.packitem.PackItemActivity
import com.example.trapp.ui.place.PlaceActivity
import com.example.trapp.ui.plan.AddPlanActivity
import com.example.trapp.ui.plan.PlanActivity
import com.example.trapp.ui.profile.ProfileActivity
import com.example.trapp.ui.search.SearchActivity
import soup.neumorphism.NeumorphCardView
import soup.neumorphism.NeumorphImageButton
import soup.neumorphism.ShapeType


class NavbarFragment : Fragment() {

    private lateinit var parentContext: Context

    private lateinit var binding: NavbarBinding

    private lateinit var budgetButton: NeumorphImageButton
    private lateinit var homeButton: NeumorphImageButton
    private lateinit var searchButton: NeumorphImageButton
    private lateinit var packItemButton: NeumorphImageButton
    private lateinit var profileButton: NeumorphImageButton

    override fun onAttach(context: Context) {
        super.onAttach(context)
        parentContext = context
    }

    private fun initializeButtons() {
        searchButton.setShapeType(ShapeType.BASIN)
        packItemButton.setShapeType(ShapeType.BASIN)
        profileButton.setShapeType(ShapeType.BASIN)
        budgetButton.setShapeType(ShapeType.BASIN)
        homeButton.setShapeType(ShapeType.BASIN)


        if (activity!! !is SearchActivity) {
            addListener(SEARCH_ACTIVITY)
            searchButton.setShapeType(ShapeType.FLAT)
        }
        if (activity!! !is BudgetActivity) {
            budgetButton.setShapeType(ShapeType.FLAT)
            addListener(BUDGET_ACTIVITY)
        }
        if (activity!! !is PlanActivity) {
            homeButton.setShapeType(ShapeType.FLAT)
            addListener(HOME_ACTIVITY)
        }
        if (activity!! !is PackItemActivity) {
            packItemButton.setShapeType(ShapeType.FLAT)
            addListener(PACK_ITEM_ACTIVITY)
        }
        if (activity!! !is ProfileActivity) {
            profileButton.setShapeType(ShapeType.FLAT)
            addListener(PROFILE_ACTIVITY)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = NavbarBinding.inflate(layoutInflater)
        val view = binding.root
        budgetButton = binding.budgetButton
        homeButton = binding.homeButton
        searchButton = binding.searchButton
        packItemButton = binding.maletaButton
        profileButton = binding.profileButton
        initializeButtons()
        val btnAddPlan = binding.addPlanButton
        btnAddPlan.setOnClickListener {
            val intent = Intent(activity, AddPlanActivity::class.java)
            startActivity(intent)
        }

        showButton()
        return view
    }

    private fun showButton() {
        if (parentContext is PlanActivity) {
            binding.addPlanButton.visibility = View.VISIBLE
            binding.createPlanButton.visibility = View.GONE
        } else if (parentContext is AddPlanActivity) {

            binding.addPlanButton.visibility = View.GONE
            binding.createPlanButton.visibility = View.VISIBLE
        } else {
            binding.addPlanButton.visibility = View.GONE
            binding.createPlanButton.visibility = View.GONE
        }

    }


private fun addListener(activityName: String) {

    when (activityName) {
        SEARCH_ACTIVITY -> {
            searchButton.setOnClickListener {
                val intent: Intent = Intent(activity!!, SearchActivity::class.java)
                startActivity(intent)
            }

        }
        PACK_ITEM_ACTIVITY -> {
            packItemButton.setOnClickListener {
                val intent: Intent = Intent(activity!!, PackItemActivity::class.java)
                activity!!.startActivityFromFragment(this, intent, 0)
                startActivity(intent)
            }


        }
        PROFILE_ACTIVITY -> {
            profileButton.setOnClickListener {
                val intent: Intent = Intent(activity!!, ProfileActivity::class.java)
                startActivity(intent)
            }
        }
        BUDGET_ACTIVITY -> {
            budgetButton.setOnClickListener {
                val intent: Intent = Intent(activity!!, BudgetActivity::class.java)
                startActivity(intent)
            }

        }

        HOME_ACTIVITY -> {
            homeButton.setOnClickListener {
                val intent: Intent = Intent(activity!!, PlanActivity::class.java)
                startActivity(intent)
            }
        }


    }
}

}