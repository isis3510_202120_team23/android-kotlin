package com.example.trapp.data.place

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.Place
import com.example.trapp.data.model.PlaceReview
import com.example.trapp.data.model.Review
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class PlaceRepository private constructor(
    private val placeDAO: PlaceDAO,
    private val placeRoomDAO: PlaceRoomDAO
) {

    suspend fun getPlaces(): LiveData<List<Place>> {

        val placesRoomLive = MutableLiveData<List<Place>>()

        val placesRoom = withContext(Dispatchers.IO) { placeRoomDAO.getAll() }


        if (placesRoom.isNotEmpty()) {
            if (Calendar.getInstance().timeInMillis - placesRoom[0].saveDate!! < 6.048e+8) {
                placesRoomLive.postValue(placesRoom)
                return placesRoomLive
            }
            //skip
        }

        val places = placeDAO.getPlaces()

        withContext(Dispatchers.IO) { placeRoomDAO.deleteAll() }

        for (place in places.value!!) {
            place.saveDate = Calendar.getInstance().timeInMillis
            withContext(Dispatchers.IO) { placeRoomDAO.insertAll(place) }
        }

        return places

    }

    suspend fun getPlaceById(placeId: String): LiveData<Place> {

        val place = placeDAO.getPlaceById(placeId)

        return place
    }

    suspend fun addNewReview(placeId: String, review: PlaceReview)
    {
        placeDAO.updateReviews(review, placeId)
    }

    companion object {
        @Volatile
        private var instance: PlaceRepository? = null

        fun getInstance(placeDAO: PlaceDAO, placeRoomDAO: PlaceRoomDAO) =
            instance ?: synchronized(this) {
                instance ?: PlaceRepository(placeDAO, placeRoomDAO).also { instance = it }
            }
    }


}