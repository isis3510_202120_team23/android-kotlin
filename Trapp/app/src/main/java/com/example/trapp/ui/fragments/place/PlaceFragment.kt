package com.example.trapp.ui.fragments.place

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.util.Pair
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trapp.R
import com.example.trapp.data.model.Place
import com.example.trapp.databinding.FragmentFoundBinding
import com.example.trapp.databinding.FragmentNotFoundBinding
import com.example.trapp.databinding.FragmentSearchResultBinding
import com.example.trapp.ui.place.GPSAdapter
import com.example.trapp.ui.place.PlaceViewModel
import com.example.trapp.ui.place.PlacesViewModelFactory
import com.example.trapp.ui.search.SearchActivity
import com.example.trapp.ui.search.SearchViewModel
import com.example.trapp.utilities.InjectorUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.ArrayList

class PlaceFragment : Fragment() {

    private lateinit var binding:FragmentSearchResultBinding

    private lateinit var parentContext: SearchActivity

    private var placeList: ArrayList<Place> = ArrayList<Place>()

    private lateinit var recycler: RecyclerView

    private lateinit var foundScreenBinding: FragmentFoundBinding

    private lateinit var notfoundScreenBinding: FragmentNotFoundBinding

//    private var param1: String? = null
//    private var param2: String? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is SearchActivity){
        parentContext = context
        }
        else {
            throw RuntimeException("$context mal")
        }
    }

    private fun setupViewModels() {

        val searchViewModel: SearchViewModel = InjectorUtils.provideSearchViewModelSingleton(parentContext)

        searchViewModel.getPlacesResults().observe(viewLifecycleOwner) { ids ->

                CoroutineScope(Dispatchers.IO).launch {
                    loadResults(ids)
                }


        }

    searchViewModel.getSearchTerm().observe(viewLifecycleOwner){
            if(it.isNullOrEmpty()){
                foundScreenBinding.root.visibility = View.GONE
                notfoundScreenBinding.root.visibility = View.VISIBLE
                notfoundScreenBinding.notFoundText.text = getString(R.string.firstSearch)
                notfoundScreenBinding.imageSearch.setImageDrawable(getDrawable(parentContext, R.drawable.ic_place_black_24dp))
            }
            else{
                foundScreenBinding.root.visibility = View.VISIBLE
                notfoundScreenBinding.root.visibility = View.GONE
            }
        }
    }

    private fun loadResults(places: List<Place>) {

        CoroutineScope(Dispatchers.Default).launch {
            placeList.clear()
            Log.d("TAG", "loadPlaceResults: ${places.size}")

            places.forEach { place ->
                placeList.add(place)
            }
            CoroutineScope(Dispatchers.Main).launch {
                if(places.isNotEmpty()){
                    foundScreenBinding.foundScreen.visibility = View.VISIBLE
                    notfoundScreenBinding.notfoundScreen.visibility = View.GONE

                }
                else{
                    foundScreenBinding.foundScreen.visibility = View.GONE
                    notfoundScreenBinding.notfoundScreen.visibility = View.VISIBLE
                    notfoundScreenBinding.notFoundText.text = getString(R.string.noResults)
                    notfoundScreenBinding.imageSearch.setImageDrawable(getDrawable(parentContext, R.drawable.ic_search_off_black_24dp))
                }
                recycler.adapter?.notifyDataSetChanged()
            }
        }
    }

    private fun setUpRecycler(recycler: RecyclerView?, adapter: RecyclerView.Adapter<*>) {
        if (recycler != null) {
            val layoutManager = GridLayoutManager(parentContext, 2)
            recycler.layoutManager = layoutManager
            recycler.adapter = adapter as RecyclerView.Adapter<*>
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
//            param1 = it.getString("")
//            param2 = it.getString("")
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSearchResultBinding.inflate(layoutInflater, container, false)
        val view = binding.root
        val itemAdapter = GPSAdapter(placeList)

        recycler = binding.foundScreen.searchResultRecycler
        setUpRecycler(recycler, itemAdapter)
        lifecycleScope.launch {
            setupViewModels()
        }
        foundScreenBinding = binding.foundScreen
        notfoundScreenBinding = binding.notfoundScreen

        return  view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PackItemFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(/* param1: String, param2: String */) =
            PlaceFragment().apply {
//                arguments = Bundle().apply {
//                    putString("", param1)
//                    putString("", param2)
//                }
            }
    }
}