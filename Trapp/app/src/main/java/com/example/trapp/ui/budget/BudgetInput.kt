package com.example.trapp.ui.budget

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import com.example.trapp.R


class BudgetInput : AppCompatDialogFragment() {
    private var listener: ExampleDialogListener? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        val view: View = inflater.inflate(R.layout.layout_dialog, null)

      val editTextUsername = view.findViewById<EditText>(R.id.edit_budget)
        editTextUsername.inputType =
            InputType.TYPE_CLASS_NUMBER

        return AlertDialog.Builder(requireActivity())
            .setView(view)
            .setTitle("Change your budget")
            .setNegativeButton("Cancel") {_, _, ->}
            .setPositiveButton("Ok") {_, _, ->
                val username = editTextUsername.text.toString()
                listener!!.applyTexts(username)
            }
            .create()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = try {
            context as ExampleDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException(
                context.toString() +
                        "must implement ExampleDialogListener"
            )
        }
    }

    interface ExampleDialogListener {
        fun applyTexts(username: String?)
    }
}