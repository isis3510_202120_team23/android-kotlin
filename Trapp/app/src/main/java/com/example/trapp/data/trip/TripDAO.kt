package com.example.trapp.data.trip



import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.Review
import com.example.trapp.data.model.Trip
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import com.google.firebase.perf.ktx.performance

class TripDAO {

    private val db = Firebase.firestore
    private val tripsdb = "trips"

    private val tripList = mutableListOf<Trip>()
    // origins<cityId,List<tripId>>
    // destinations<cityId,List<tripId>>
    private val originMap = mutableMapOf<String, MutableList<String>>()
    private val destinationMap = mutableMapOf<String, MutableList<String>>()

    private val trips = MutableLiveData<List<Trip>>()
    // origins<cityId,List<tripId>>
    // destinations<cityId,List<tripId>>
    private val origins = MutableLiveData<Map<String, List<String>>>()
    private val destinations = MutableLiveData<Map<String, List<String>>>()


    init {
        trips.value = tripList
        origins.value = originMap
        destinations.value = destinationMap
    }

    companion object {
        fun newInstance() = TripDAO()
    }

    fun getAllTrips(): List<Trip> {
        val trips: MutableList<Trip> = emptyList<Trip>().toMutableList()
        db.collection(tripsdb)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    //Log.d(tripsdb, "${document.id} => ${document.data}")
                    trips.add(
                        Trip(
                            document.id,
                            document.data.getValue("img") as String,
                            document.data.getValue("name") as String,
                            document.data.getValue("price") as Long,
                            document.data.getValue("startDate") as String,
                            document.data.getValue("endDate") as String
                        )
                    )
                }
            }
            .addOnFailureListener { exception ->
                Log.w(tripsdb, "Error getting documents: ", exception)
            }
        return trips
    }

    private suspend fun getAllTripsAsync(){withContext(Dispatchers.IO) {
       try {
           val myTrace = Firebase.performance.newTrace("Get all Trips")
           myTrace.start()
            val data = db.collection(tripsdb)
                .get()
                .await()

            for (document in data.documents) {
                val trip=Trip(
                    document.id,
                    document.data?.getValue("img") as String,
                    document.data?.getValue("name") as String,
                    document.data!!.getValue("price") as Long,
                    document.data!!.getValue("startDate") as String,
                    document.data!!.getValue("endDate") as String
                )
                tripList.add(
                    trip
                )
                // Link origins and destinations
                withContext(Dispatchers.IO) {
                    val destinationReference : DocumentReference = document.data?.get("destination") as DocumentReference
                    val originReference : DocumentReference = document.data?.get("origin") as DocumentReference
                    val destinationId = destinationReference.id
                    val originId = originReference.id

                    if(!originMap.containsKey(originId)) {
                        originMap[originId] = mutableListOf<String>()
                    }
                    if(!destinationMap.containsKey(destinationId)) {
                        destinationMap[destinationId] = mutableListOf<String>()
                    }

                    originMap[originId]?.add(trip.id)
                    destinationMap[destinationId]?.add(trip.id)
                }
            }
            tripList
           myTrace.stop()
        } catch (e: Exception) {
            //Log.d("tripDAO",e.stackTraceToString())
        }
    }
    }

    suspend fun getTripsByNameAsync(tripName: String): MutableList<Trip> =
        withContext(Dispatchers.IO)
        {
            // podrían repetir nombres
            val trips: MutableList<Trip> = emptyList<Trip>().toMutableList()
            return@withContext try {
                val data = db.collection(tripsdb).whereEqualTo("name", tripName)
                    .get()
                    .await()

                for (document in data.documents) {
                    trips.add(
                        Trip(
                            document.id,
                            document.data?.getValue("img") as String,
                            document.data?.getValue("name") as String,
                            document.data?.getValue("price") as Long,
                            document.data?.getValue("startDate") as String,
                            document.data?.getValue("endDate") as String
                        )
                    )
                }
                trips
            } catch (e: Exception) {
                //Log.d("packitemactivity",e.stackTraceToString())

                trips
            }
        }

    suspend fun getTripByIdAsync(tripId: String): Trip? = withContext(Dispatchers.IO)
    {
        var value: Trip? = null
        return@withContext try {
            val document = db.collection(tripsdb).document(tripId)
                .get().await()

            value = Trip(
                document.id,
                document.data?.getValue("img") as String,
                document.data?.getValue("name") as String,
                document.data!!.getValue("price") as Long,
                document.data!!.getValue("startDate") as String,
                document.data!!.getValue("endDate") as String
            )
            value
        } catch (e: Exception) {
            //Log.d("packitemactivity",e.stackTraceToString())

            value
        }
    }

    suspend fun getTripDestinationIdAsync(tripId: String?): String? = withContext(Dispatchers.IO)
    {
        return@withContext try {
            val myTrace = Firebase.performance.newTrace("Get Trip from Destination Async")
            myTrace.start()
            if(tripId !=null){
            val trip = db.collection(tripsdb).document(tripId)
                .get().await()

            val cityReference: DocumentReference =
                trip.data?.get("destination") as DocumentReference
            myTrace.stop()
            cityReference.id}
            else{
                null
            }
        } catch (e: Exception) {
            //Log.d("packitemactivity",e.stackTraceToString())

            null
        }
    }

    suspend fun getTripReviewsAsync(tripId: String): MutableList<Review> =
        withContext(Dispatchers.IO)
        {
            val reviews: MutableList<Review> = emptyList<Review>().toMutableList()
            return@withContext try {
                val myTrace = Firebase.performance.newTrace("Get Trip reviews")
                myTrace.start()
                val trip = db.collection(tripsdb).document(tripId).get().await()
                val documents = trip.data?.get("reviews") as ArrayList<*>
                for (review in documents) {
                    review as Map<String, *>
                    reviews.add(
                        Review(
                            review["califiction"] as Double,
                            review["rationale"] as String
                        )
                    )
                }
                myTrace.stop()
                reviews
            } catch (e: Exception) {
                //Log.d("suggested", e.stackTraceToString())
                reviews
            }
        }

    suspend fun getSuggestedItemsIdsTripAsync(tripId: String): MutableList<String> =
        withContext(Dispatchers.IO)
        {
            val items: MutableList<String> = mutableListOf<String>()
            return@withContext try {
                val myTrace = Firebase.performance.newTrace("Get Trip Suggested items")
                myTrace.start()
                val trip = db.collection(tripsdb).document(tripId)
                    .get()
                    .await()

                val itemPath = trip.data?.getValue("packitem") as DocumentReference
                val itemId = itemPath.id
                items.add(itemId)
                myTrace.stop()
                items
            } catch (e: Exception) {
                Log.d("packitemactivity",e.stackTraceToString())
                items
            }
        }


    fun createTrip(
        tripId: String,
        name: String,
        price: Long,
        startDate: String,
        endDate: String
    ) {
        val data = hashMapOf(
            "name" to name,
            "price" to price,
            "startDate" to startDate,
            "endDate" to endDate
        )
        db.collection(tripsdb).document(tripId).set(data).addOnSuccessListener {
            //Log.d(tripsdb, "DocumentSnapshot written with ID: $tripId")
        }
            .addOnFailureListener { e ->
                Log.w(tripsdb, "Error adding document", e)
            }
    }

    fun updateTrip(trip: Trip) {
        createTrip(trip.id, trip.name, trip.price, trip.startDate, trip.endDate)
    }

    fun deleteTripByName(tripName: String) {
        val documentPath = db.collection(tripsdb).whereEqualTo("name", tripName)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    //Log.d(tripsdb, "${document.id} => ${document.data}")
                    document.reference.delete()
                        .addOnSuccessListener {
                            Log.d(
                                tripsdb,
                                "DocumentSnapshot successfully deleted!"
                            )
                        }
                        .addOnFailureListener { e ->
                            Log.w(
                                tripsdb,
                                "Error deleting document",
                                e
                            )
                        }
                }
            }

        return
    }

    fun deleteTrip(tripId: String) {
        db.collection(tripsdb).document(tripId).delete()
        return
    }


    suspend fun getTrips(): LiveData<List<Trip>> {
        getAllTripsAsync()
        return trips
    }

    fun getOriginLinkTrip(): LiveData<Map<String, List<String>>> {
        return origins
    }

    fun getDestinationLinkTrip(): LiveData<Map<String, List<String>>> {
        return destinations
    }

    fun getTripDestination(tripId:String): String? {
        var destinationId:String? = null
        for(tuple in destinations.value?.entries!!)
        {
            if(tuple.value.contains(tripId))
            {
                destinationId = tuple.key
            }
        }
        return destinationId
    }
}