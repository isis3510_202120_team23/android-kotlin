package com.example.trapp.ui.plan

import androidx.lifecycle.ViewModel
import com.example.trapp.data.model.Plan
import com.example.trapp.data.plan.PlanMarioRepository
import com.example.trapp.data.plan.PlanRepository

class PlansViewModel (private val planRepository: PlanRepository) : ViewModel(){

    suspend fun getPlans()=planRepository.getPlans()
    suspend fun createPlan(plan: Plan)=planRepository.createPlan(plan)

}
