package com.example.trapp.ui.packitem

import android.content.Context
import com.example.trapp.*
import com.example.trapp.data.model.*
import com.example.trapp.utilities.*
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.google.firebase.perf.metrics.AddTrace
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import android.util.Pair
import com.example.trapp.databinding.ActivityPackItemBinding
import com.example.trapp.databinding.NoInternetScreenBinding
import com.example.trapp.ui.fragments.navbar.NavbarFragment

class PackItemActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPackItemBinding
    private lateinit var offlineBinding: NoInternetScreenBinding

    private var itemList: ArrayList<Pair<Boolean, PackItem>> = ArrayList<Pair<Boolean, PackItem>>()

    private lateinit var recycler: RecyclerView

    private val db = Firebase.firestore

    private lateinit var initialTime: Calendar

    private var timeLoadingInit = Calendar.getInstance().timeInMillis

    private var isNetworkConnected = false

    /*
     * Activity lifecycle
     */
    val trace = Firebase.performance.newTrace("Loading time packItem Activity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPackItemBinding.inflate(layoutInflater)
        offlineBinding = NoInternetScreenBinding.inflate(layoutInflater)
        setView(savedInstanceState)
        trace.start()
        timeLoadingInit = Calendar.getInstance().timeInMillis
        isNetworkConnected = isOnline()
        addNetworkCallback()
    }

    override fun onStart() {
        super.onStart()
        trace.stop()
        writeLoadingTime()
    }

    override fun onStop() {
        super.onStop()
        writeNewTime()
    }

    /*
     *  UI
     */
    private fun setView(savedInstanceState: Bundle?) {
        val view = binding.root
        val offlineView = offlineBinding.root
        if (isOnline()) {
            setContentView(view)
            binding.toploadingbar.visibility = View.VISIBLE
            loadUiOnline()
        } else {
            setContentView(offlineView)
            val view: View = window.decorView.rootView
            val snackbar = Snackbar.make(view, R.string.nointernettext, Snackbar.LENGTH_INDEFINITE)
                .setAnchorView(R.id.snackbarDummy)
            snackbar.show()
            val retryButton: AppCompatButton = offlineBinding.RetryButton
            retryButton.setOnClickListener { setView(savedInstanceState) }
        }
        addNavbar(savedInstanceState)
    }

    private fun loadUiOnline() {
        val tripId: String? = intent.extras?.getString("trip")
        binding.toploadingbar.visibility = View.VISIBLE
//        val fab: FloatingActionButton? = findViewById(R.id.items_fab)
//        fab?.setOnClickListener {
//            //TODO createItem()
//        }


        initializePackItemUi(tripId)


        val itemAdapter = PackItemsAdapter(itemList)
        recycler = binding.itemRecyclerView
        setUpRecycler(recycler, itemAdapter)
    }

    private fun setUpRecycler(recycler: RecyclerView?, adapter: Any) {
        if (recycler != null) {
            val layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            recycler.layoutManager = layoutManager
            recycler.adapter = adapter as RecyclerView.Adapter<*>
        }
    }

    @AddTrace(name = "InitializeUi pack item activity", enabled = true)
    private fun initializePackItemUi(tripId: String?) {
        reloadItems(tripId)
    }

    private fun reloadItems(tripId: String?) {
        val factory = InjectorUtils.providePackItemsViewModelFactory()
        val viewModel = ViewModelProvider(this, factory).get(PackItemViewModel::class.java)

        viewModel.getDisplayPackItems(tripId).observe(this) { displayItems ->

            CoroutineScope(Dispatchers.Default).launch {

                if (displayItems.containsKey(tripId)){
                    itemList.clear()
                    itemList.addAll(displayItems[tripId]!!)
                    CoroutineScope(Dispatchers.Main).launch {
                        recycler.adapter?.notifyDataSetChanged()
                        binding.toploadingbar.visibility = View.GONE
                    }
                }
            }
        }
    }

    //
    private fun addNavbar(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                val fragment = NavbarFragment()
                replace(R.id.navbar_fragment, fragment, "NavbarFragment")
            }
        }
    }

    /*
     *  Lists and arrays
     */
//    private fun swap(
//        array: ArrayList<MutablePair<Boolean, PackItem>>,
//        indexA: Int,
//        indexB: Int
//    ) {
//        val temp: MutablePair<Boolean, PackItem> = array[indexA]
//        val firstId: String = array[indexA].second.itemId
//        val secondId: String = array[indexB].second.itemId
//        itemMap[firstId] = indexB
//        itemMap[secondId] = indexA
//        array[indexA] = array[indexB]
//        array[indexB] = temp
//    }

    /*
     * Networking and connectivity related functions
     */
    private fun isOnline(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

    private fun isMetered(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && cm.isActiveNetworkMetered
    }

    private fun addNetworkCallback() {

        val cm: ConnectivityManager =
            application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder: NetworkRequest.Builder = NetworkRequest.Builder()

        val networkCallback = object : ConnectivityManager.NetworkCallback() {

            override fun onLost(network: Network) {
                isNetworkConnected = false
                Snackbar.make(
                    window.decorView.rootView,
                    R.string.nointernettext,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAnchorView(R.id.snackbarDummy).show()
            }

            override fun onUnavailable() {
                isNetworkConnected = false
                Snackbar.make(
                    window.decorView.rootView,
                    R.string.nointernettext,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAnchorView(R.id.snackbarDummy).show()
            }

            override fun onAvailable(network: Network) {

                if (!isNetworkConnected) {
                    val snackbar = Snackbar.make(
                        window.decorView.rootView, R.string.backonline,
                        Snackbar.LENGTH_SHORT
                    )
                        .setAnchorView(R.id.snackbarDummy)
                        .setBackgroundTint(getColor(R.color.primary))
                        .setTextColor(getColor(R.color.black)).setAnchorView(R.id.snackbarDummy)
                    snackbar.show()
                }
                isNetworkConnected = true
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            cm.registerDefaultNetworkCallback(networkCallback)
        } else {
            cm.registerNetworkCallback(
                builder.build(), networkCallback
            )
        }
    }

    /*
     * Time measuring and logging
     */

    private fun writeNewTime() {
        CoroutineScope(Dispatchers.Default).launch {
            val finalTime = Calendar.getInstance()
            val millis1: Long = initialTime.timeInMillis
            val millis2: Long = finalTime.timeInMillis
            // Calculate difference in milliseconds
            val diff = millis2 - millis1
            // Calculate difference in seconds
            val timeSpent = diff / 1000
            //save to database...
            val userTime = hashMapOf(
                "activity" to "PackItem",
                "time" to timeSpent
            )
            CoroutineScope(Dispatchers.IO).launch {
                db.collection("userTime").add(userTime)
            }
        }
    }

    private fun writeLoadingTime() {
        initialTime = Calendar.getInstance()
        val timeLoadingEnd = Calendar.getInstance().timeInMillis
        CoroutineScope(Dispatchers.Default).launch {
            val elapsedTime = timeLoadingEnd - timeLoadingInit
            val userTime = hashMapOf(
                "activity" to "Packing",
                "time" to elapsedTime
            )
            CoroutineScope(Dispatchers.IO).launch {
                db.collection("loadingTime").add(userTime)

            }
        }
    }
}