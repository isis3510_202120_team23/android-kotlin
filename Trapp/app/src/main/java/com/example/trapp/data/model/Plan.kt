package com.example.trapp.data.model

class Plan (
    val city: City? = null,
    val name: String? = null,
    val userGroup: UserGroup? = null,
    var month: Int?=null
)