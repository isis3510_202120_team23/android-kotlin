package com.example.trapp.data.trip

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.Place
import com.example.trapp.data.model.Trip
import com.example.trapp.data.place.PlaceRoomDAO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class TripRepository private constructor(private val tripDAO: TripDAO, private val tripRoomDao:TripRoomDAO) {

   //suspend fun getTrips() = tripDAO.getTrips()
   /*fun getOriginLinkTrip() = tripDAO.getOriginLinkTrip()
    fun getDestinationLinkTrip() = tripDAO.getDestinationLinkTrip()
    fun getTripDestination(tripId:String) = tripDAO.getTripDestination(tripId)
    suspend fun getTripDestinationIdAsync(tripId:String?) = tripDAO.getTripDestinationIdAsync(tripId)*/
    suspend fun getTrips(): LiveData<List<Trip>> {

        val tripsRoomLive = MutableLiveData<List<Trip>>()

        val tripsRoom = withContext(Dispatchers.IO) {tripRoomDao.getAll()}

        Log.d("Trips ROOM", tripsRoom.toString())
        if(tripsRoom.isNotEmpty())
        {
            if(Calendar.getInstance().timeInMillis - tripsRoom[0].saveDate!! < 6.048e+8)
            {
                tripsRoomLive.postValue(tripsRoom)
                return tripsRoomLive
            }
            //skip
        }

        val trips = tripDAO.getTrips()

        withContext(Dispatchers.IO){tripRoomDao.deleteAll()}

        for(trip in trips.value!!)
        {
            trip.saveDate = Calendar.getInstance().timeInMillis
            withContext(Dispatchers.IO){tripRoomDao.insertAll(trip)}
        }

        return trips

    }
    companion object {
        @Volatile
        private var instance: TripRepository? = null

        fun getInstance(tripDAO: TripDAO, tripRoomDao: TripRoomDAO) =
            instance ?: synchronized(this) {
                instance ?: TripRepository(tripDAO, tripRoomDao).also { instance = it }
            }
    }

}