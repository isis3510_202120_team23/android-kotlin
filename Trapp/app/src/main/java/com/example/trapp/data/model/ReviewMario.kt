package com.example.trapp.data.model

data class ReviewMario(
    //calification no es una palabra en inglés xd
    val calification: Double? = null,
    val rationale: String? = null
    )