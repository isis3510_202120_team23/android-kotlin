package com.example.trapp.ui.user

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.user.UserRepository

class UsersViewModelFactory (private val UserRepository: UserRepository): ViewModelProvider.NewInstanceFactory() {


    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return UsersViewModel(UserRepository) as T
    }

}
