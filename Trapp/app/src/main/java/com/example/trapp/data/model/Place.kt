package com.example.trapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class Place(
    @PrimaryKey var firebaseId:String,
    @ColumnInfo(name = "address") var address: String,
    @ColumnInfo(name = "img")var img: String,
    @ColumnInfo(name = "latitude")var latitude: Double,
    @ColumnInfo(name = "longitude")var longitude: Double,
    @ColumnInfo(name = "name")var name:String,
    @ColumnInfo(name = "save_date") var saveDate: Long?,
    @Ignore var reviews: List<PlaceReview>?,
    var overview: String?
)
{
    constructor(firebaseId: String, address: String, img: String, latitude: Double, longitude: Double, name: String): this(firebaseId, address, img, latitude, longitude, name, null, null, null) {
    }
}
