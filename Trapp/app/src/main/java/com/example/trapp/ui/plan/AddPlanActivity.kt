package com.example.trapp.ui.plan

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.view.View.INVISIBLE
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trapp.R
import com.example.trapp.data.Contact.ContactViewModel
import com.example.trapp.data.model.*
import com.example.trapp.databinding.ActivityAddPlanBinding
import com.example.trapp.databinding.NavbarBinding
import com.example.trapp.databinding.NoInternetScreenBinding
import com.example.trapp.ui.fragments.navbar.NavbarFragment
import com.example.trapp.ui.user.UsersViewModel
import com.example.trapp.utilities.Connectivity
import com.example.trapp.utilities.InjectorUtils
import com.google.android.material.radiobutton.MaterialRadioButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*


class AddPlanActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddPlanBinding
    private lateinit var offlinebinding: NoInternetScreenBinding
    private lateinit var navbarbinding: NavbarBinding


    private var usersInDB: MutableList<User> = emptyList<User>().toMutableList()
    var contactAdapter: ContactsAdapter? = null
    var userGroup: UserGroup? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAddPlanBinding.inflate(layoutInflater)
        navbarbinding= NavbarBinding.inflate(layoutInflater)

        offlinebinding = NoInternetScreenBinding.inflate(layoutInflater)
        setView(savedInstanceState)


    }
    private fun getInternet(): Boolean {
        var connectivity = Connectivity.getInstance()
        return connectivity.getInternet(applicationContext)
    }
    private fun setView(savedInstanceState: Bundle?) {
        val view = binding.root
        val offlineView = offlinebinding.root
        if (getInternet()) {
            setContentView(view)
            initialize()
        } else {
            setContentView(offlineView)
            val view: View = window.decorView.rootView
            val snackbar = Snackbar.make(view, R.string.nointernettext, Snackbar.LENGTH_INDEFINITE)
                .setAnchorView(R.id.snackbarDummy)
            snackbar.show()
            val retryButton: AppCompatButton = offlinebinding.RetryButton
            retryButton.setOnClickListener { setView(savedInstanceState) }
        }
        addNavbar(savedInstanceState)
    }
private fun initialize()
{
    val view = binding.root
    val userFactory = InjectorUtils.provideUsersViewModelFactory()
    val userViewModel =
        ViewModelProviders.of(this, userFactory).get(UsersViewModel::class.java)
    val contactFactory = InjectorUtils.provideContactsViewModelFactory(applicationContext)
    val contactViewModel =
        ViewModelProviders.of(this, contactFactory).get(ContactViewModel::class.java)
    setContentView(view)
    //  usersInDB= userViewModel.getUsers()
    val coroutineScope = CoroutineScope(Dispatchers.Main)
    coroutineScope.launch {
        val user = Firebase.auth.currentUser
        var contactsCompilant: MutableList<Contact> = emptyList<Contact>().toMutableList()
        var usuariosAdapter: MutableList<User> = emptyList<User>().toMutableList()
        val deferred: Deferred<Boolean> = coroutineScope.async {
            var contactos = contactViewModel.getContacts()
            contactsCompilant.addAll(contactos)
            true
        }

        if (deferred.await()) {
            val contactRecycler: RecyclerView = binding.friendsRecycler
            val loading: ProgressBar = binding.toploadingbarContact
            if (user != null) {
                userGroup = UserGroup(user.uid.toString() + "userGroup", 1)
            }
            contactAdapter =
                userGroup?.let { ContactsAdapter(contactsCompilant, this@AddPlanActivity, it) }
            val layoutManager = LinearLayoutManager(applicationContext)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            contactRecycler.layoutManager = layoutManager
            contactRecycler.adapter = contactAdapter
            loading.visibility = INVISIBLE

        }
    }
}


    @SuppressLint("Range")
    private fun getContactList(): MutableList<Contact> {
        var contactList: MutableList<Contact> = emptyList<Contact>().toMutableList()
        val cr = contentResolver
        val cur = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )
        if (cur?.count ?: 0 > 0) {
            while (cur != null && cur.moveToNext()) {
                val id = cur.getString(
                    cur.getColumnIndex(ContactsContract.Contacts._ID)
                )
                val name = cur.getString(
                    cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )
                if (cur.getInt(
                        cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER
                        )
                    ) > 0
                ) {
                    val pCur = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    while (pCur!!.moveToNext()) {
                        val phoneNo = pCur.getString(
                            pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER
                            )
                        )
                        Log.i(TAG, "Name: $name")
                        Log.i(TAG, "Phone Number: $phoneNo")
                        val contact = Contact(5,id, name, phoneNo)
                        contactList.add(contact)
                    }
                    pCur.close()
                }

            }
        }
        cur?.close()
        return contactList
    }

    override fun onStart() {
        super.onStart()

        val btnCreatePlanSubmit = findViewById<Button>(R.id.createPlanButton)
        val planFactory = InjectorUtils.providePlansViewModelFactory()
        val planViewModel =
            ViewModelProviders.of(this, planFactory).get(PlansViewModel::class.java)
        btnCreatePlanSubmit.setOnClickListener {
            val city = binding.editTextPickTheCity.text.toString()
            val name = binding.editTextPickPlanName.text.toString()
            Log.d("pelota", "lo oprimieron --- se creo plan :P --- en un futuro XD")
            lifecycleScope.launch(Dispatchers.Main) {
                planViewModel.createPlan(
                    Plan(
                        City(
                            name + "city" + city,
                            5.2545564,
                            4.4564,
                            city,
                            null
                        ), name, userGroup
                    )
                )
            }
            Log.d("boton", "lo oprimieron --- se creo plan :P --- en un futuro XD")
            binding.editTextPickTheCity.setText("")
            binding.editTextPickPlanName.setText("")
            Toast.makeText(
                this,
                "Plan " + name + " successfully created",
                Toast.LENGTH_LONG
            ).show()
        }
        // setClick()

    }
    private fun addNavbar(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                val fragment = NavbarFragment()
                replace(R.id.navbar_fragment, fragment, "NavbarFragment")
            }
        }
    }
//
//    fun setClick() {
//        val userFactory = InjectorUtils.provideUsersViewModelFactory()
//        val userViewModel =
//            ViewModelProviders.of(this, userFactory).get(UsersViewModel::class.java)
//        val radioButton: RadioButton = findViewById(R.id.radioButton)
//       // var contactos = getContactList()
//        radioButton.setOnClickListener {
//            Log.d("nombre", radioButton.text as String)
//
//        }
//    }

}