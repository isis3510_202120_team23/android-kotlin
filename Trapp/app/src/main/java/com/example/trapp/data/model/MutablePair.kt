package com.example.trapp.data.model

data class MutablePair<A,B>(var first: A, var second: B)
