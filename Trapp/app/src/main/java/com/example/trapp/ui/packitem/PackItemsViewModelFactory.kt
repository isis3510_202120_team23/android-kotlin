package com.example.trapp.ui.packitem

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.packitem.PackItemRepository

class PackItemsViewModelFactory(private val packItemRepository: PackItemRepository): ViewModelProvider.NewInstanceFactory()  {

    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return PackItemViewModel(packItemRepository) as T
    }

}