package com.example.trapp.utilities

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import androidx.annotation.RequiresApi

class Connectivity {

    var isNetworkConnected: Boolean = false

    fun addNetworkCallback(context:Context) {

        val cm: ConnectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder: NetworkRequest.Builder = NetworkRequest.Builder()

        val networkCallback = object : ConnectivityManager.NetworkCallback() {

            override fun onLost(network: Network) {
         //       isNetworkConnected = false
           //     com.google.android.material.snackbar.Snackbar.make(
            //        window.decorView.rootView,
           //         R.string.nointernettext,
           //         com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE
              //  )
              //      .setAnchorView(R.id.navbar_fragment).show()
            }

            override fun onUnavailable() {/*
                isNetworkConnected = false
                com.google.android.material.snackbar.Snackbar.make(
                    window.decorView.rootView,
                    R.string.nointernettext,
                    com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE
                )
                    .setAnchorView(R.id.navbar_fragment).show()
           */ }

            @RequiresApi(Build.VERSION_CODES.M)
            override fun onAvailable(network: Network) {/*

                if (!isNetworkConnected) {
                    val snackbar = com.google.android.material.snackbar.Snackbar.make(
                        window.decorView.rootView, R.string.backonline,
                        com.google.android.material.snackbar.Snackbar.LENGTH_SHORT
                    )
                        .setAnchorView(R.id.navbar_fragment)
                        .setBackgroundTint(getColor(R.color.primary))
                        .setTextColor(getColor(R.color.black))
                        .setAnchorView(R.id.navbar_fragment)
                    snackbar.show()
                }
                isNetworkConnected = true
           */ }
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            cm.registerDefaultNetworkCallback(networkCallback)
        } else {
            cm.registerNetworkCallback(
                builder.build(), networkCallback
            )
        }
    }


    fun getInternet(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

    companion object{

        @Volatile
        private var instance: Connectivity? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: Connectivity().also { instance = it }
            }
    }
}