package com.example.trapp.ui.place

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.example.trapp.R
import com.example.trapp.data.model.Place
import com.example.trapp.data.model.PlaceReview
import com.example.trapp.data.model.Review
import com.example.trapp.data.model.User
import com.example.trapp.ui.budget.BudgetInput
import com.example.trapp.ui.place.fragments.FirstFragment
import com.example.trapp.ui.place.fragments.SecondFragment
import com.example.trapp.ui.place.fragments.ThirdFragment
import com.example.trapp.ui.user.UsersViewModel
import com.example.trapp.utilities.InjectorUtils
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher

class PlaceActivity : AppCompatActivity(), ReviewInput.ExampleDialogListener {

    private var placeIdReviews: String = "nullId"

    private var overview: String = "juas juas juas"

    private val currentUser = Firebase.auth.currentUser

    private var user = User("null")

    private var newReview = "Wth"

    private var newRating = 666L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place)

        lifecycleScope.launch(){
            initializeUI()
        }
        val editButton: AppCompatButton? = findViewById(R.id.editButton)
        editButton?.setOnClickListener {
            openDialog()
        }
    }

    private fun openDialog(){
        val exampleDialog = ReviewInput()
        exampleDialog.show(supportFragmentManager, "example dialog")
    }

    suspend fun initializeUI()
    {
        val placeNameText = findViewById<TextView>(R.id.placeName)

        val image = findViewById<ImageView>(R.id.placePhoto)

        val extras = intent.extras
        if (extras != null) {
            val placeId = extras.getString("id")
                if (placeId != null) {
                    placeIdReviews = placeId
            }
            val placeName = extras.getString("name")
            placeNameText.setText(placeName)
            val imgUrl = extras.getString("img")
            Glide.with(image).load(imgUrl).placeholder(R.mipmap.city3_foreground).into(image)
            overview = extras.getString("overview").toString()
        }

        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        val viewPager = findViewById<ViewPager2>(R.id.viewPager)

        viewPager.adapter = FragmentAdapter(this, placeIdReviews, overview)

        TabLayoutMediator(tabLayout, viewPager){tab, position ->
            if(position == 0)
            {
                tab.text = "Overview"
            }
            else
            {
                tab.text = "Reviews"
            }

        }.attach()

        val userFactory = InjectorUtils.provideUsersViewModelFactory()
        val userViewModel =
            ViewModelProviders.of(this, userFactory).get(UsersViewModel::class.java)
        if (currentUser != null) {
            userViewModel.getUserById(currentUser.uid).observe(this, { userFound ->
                user = userFound
            })
        }

    }

    override fun applyTexts(pReview: String?, pRating: String?) {

        if (pReview != null && pRating != null) {
            newRating = pRating.toLong()
            newReview = pReview
        }
        lifecycleScope.launch(){
            writeNewReview()
        }
    }

    suspend fun writeNewReview()
    {
        withContext(Dispatchers.IO){
            insertNewReview(PlaceReview(newRating, newReview, user.firstName + " " + user.lastName))
        }
    }

    private suspend fun insertNewReview(review: PlaceReview){

        val factoryPlace = InjectorUtils.providePlacesViewModelFactory(applicationContext)
        val viewModelPlace =
            ViewModelProviders.of(this, factoryPlace).get(PlaceViewModel::class.java)

        viewModelPlace.addNewReview(placeIdReviews, review)
    }


    class FragmentAdapter(activity: AppCompatActivity, idPlace: String, overview: String) : FragmentStateAdapter(activity)
    {
        val id = idPlace;
        val ov = overview;
        override fun getItemCount(): Int {
            return 2
        }

        override fun createFragment(position: Int): Fragment {
            return when(position){
                0 -> FirstFragment.newInstance(ov, "")
                1 -> SecondFragment.newInstance(id, "")
                else -> FirstFragment.newInstance()
            }
        }

    }
}