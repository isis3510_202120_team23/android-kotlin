package com.example.trapp.data.Contact

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.trapp.data.model.Contact

@Dao
interface ContactRoomDAO {
    @Query("SELECT * FROM contact")
    fun getAll(): List<Contact>

    @Insert
    fun insertAll(vararg contact: Contact)

    @Delete
    fun delete(contact: Contact)

    @Query("DELETE FROM contact")
    fun deleteAll()

}