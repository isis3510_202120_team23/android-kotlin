package com.example.trapp.data.search

import android.util.Log
import com.example.trapp.data.trip.TripDAO
import com.example.trapp.data.city.CityService
import com.example.trapp.data.model.PackItem
import com.example.trapp.data.model.Review
import com.example.trapp.data.model.search.Search
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext


class SearchService {

    private val db = Firebase.firestore
    private val items = "search"

    companion object {
        fun newInstance() = SearchService()
    }

    fun submitSearch(search: Search){
        CoroutineScope(Dispatchers.IO).launch {
            val millis = System.currentTimeMillis()
            val searchSubmit = hashMapOf("hadResults" to search.hadResults, "term" to search.term, "timestamp" to millis)
            db.collection(items).add(searchSubmit)
        }

    }
}