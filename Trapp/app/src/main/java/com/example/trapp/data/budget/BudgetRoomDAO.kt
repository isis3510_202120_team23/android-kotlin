package com.example.trapp.data.budget

import androidx.room.*
import com.example.trapp.data.model.Budget
import com.example.trapp.data.model.TestRoom


@Dao
    interface BudgetRoomDAO {
        @Query("SELECT * FROM budget")
        suspend fun getAll(): List<Budget>

        @Insert
        suspend fun insert(budget: Budget)

        @Update
        suspend fun update(budget:Budget)

        @Delete
        suspend fun delete(budget: Budget)
    }