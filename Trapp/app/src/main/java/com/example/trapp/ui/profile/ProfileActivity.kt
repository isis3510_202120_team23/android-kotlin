package com.example.trapp.ui.profile

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.example.trapp.R
import com.example.trapp.ui.login.StartActivity
import com.example.trapp.ui.fragments.navbar.NavbarFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class ProfileActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                val fragment = NavbarFragment()
                replace(R.id.navbar_fragment, fragment, "NavbarFragment")
            }
        }
        // Initialize Firebase Auth
        auth = Firebase.auth
        setup()
    }

    private fun setup(){

        val button = findViewById<Button>(R.id.signOutButton)

        button.setOnClickListener{
            Firebase.auth.signOut()
            val startIntent = Intent(this, StartActivity::class.java)
            startActivity(startIntent)
        }

    }
}