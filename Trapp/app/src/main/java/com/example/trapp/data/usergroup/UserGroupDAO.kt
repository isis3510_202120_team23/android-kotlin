package com.example.trapp.data.usergroup


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.UserGroup
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class UserGroupDAO {
    private val db = Firebase.firestore
    private val collectionName = "userGroupsMario"
    private val userGroupList = mutableListOf<UserGroup>()
    private var userGroup = UserGroup("nullId")

    private val userGroups = MutableLiveData<List<UserGroup>>()
    private val userGroupLive = MutableLiveData<UserGroup>()

    init {
        userGroups.value = userGroupList
        userGroupLive.value = userGroup
    }

    companion object {
        fun newInstance() = UserGroupDAO()
    }

    suspend fun getAllUserGroupsAsync(): MutableList<UserGroup> = withContext(Dispatchers.IO) {
        return@withContext try {
            val data = db.collection(collectionName)
                .get()
                .await()

            for (document in data.documents) {
                userGroupList.add(
                    UserGroup(
                        document.id,
                        document.data!!.getValue("numberMembers") as Long,
                        document.data!!.getValue("plans") as List<String>,
                        document.data!!.getValue("totalBudget") as Double
                    )
                )
            }
            userGroupList
        } catch (e: Exception) {
            userGroupList
        }
    }

    suspend fun getUserGroupByIdAsync(userGroupId: String): UserGroup = withContext(Dispatchers.IO)
    {
        return@withContext try {
            val document = db.collection(collectionName).document(userGroupId)
                .get().await()

            userGroup = UserGroup(
                document.id,
                document.data!!.getValue("numberMembers") as Long,
                document.data!!.getValue("plans") as List<String>,
                document.data!!.getValue("totalBudget") as Double
            )
            userGroup
        } catch (e: Exception) {
            println(e)
            userGroup
        }
    }

    suspend fun getUserGroups(): LiveData<List<UserGroup>> {
        getAllUserGroupsAsync()
        return userGroups
    }

    suspend fun getUserGroupById(userGroupId: String): LiveData<UserGroup> {
        getUserGroupByIdAsync(userGroupId)

        userGroupLive.value = userGroup
        return userGroupLive
    }

}
