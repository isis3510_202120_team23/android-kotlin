package com.example.trapp.ui.login

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.trapp.R
import com.example.trapp.ui.login.LoginActivity
import com.example.trapp.ui.login.SignUpActivity
import com.example.trapp.ui.plan.PlanActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.*

const val SEARCH_ACTIVITY ="Search"
const val BUDGET_ACTIVITY ="Budget"
const val HOME_ACTIVITY ="Home"
const val PACK_ITEM_ACTIVITY ="Pack Item"
const val SETTINGS_ACTIVITY ="Settings"
const val PROFILE_ACTIVITY ="Profile"


class StartActivity : AppCompatActivity() {
    private var timeLoadingInit=0L
    private var timeLoadingEnd=0L
    private val db = Firebase.firestore

    private val timeSpent = 0

    private lateinit var initialTime: Calendar

    private lateinit var startAppTime: Calendar

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Trapp)
        super.onCreate(savedInstanceState)
        startAppTime = Calendar.getInstance()
        timeLoadingInit= Calendar.getInstance().timeInMillis
        setContentView(R.layout.activity_start)
      /*  val requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {

                } else {

                }
            }
        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION )

        if (permissionCheck== PackageManager.PERMISSION_DENIED)
        {
            requestPermissionLauncher.launch(
                Manifest.permission.ACCESS_FINE_LOCATION)
        }*/
        val requestPermissionLauncherContact =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {

                } else {

                }
            }
        val permissionCheckContact = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS )

        if (permissionCheckContact== PackageManager.PERMISSION_DENIED)
        {
            requestPermissionLauncherContact.launch(
                Manifest.permission.READ_CONTACTS)
        }
        val requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {

                } else {

                }
            }
        val permissionCheck =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            requestPermissionLauncher.launch(
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
        auth = Firebase.auth
        setup()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser != null){
            val planIntent = Intent(this, PlanActivity::class.java)
            startActivity(planIntent)
        }
        initialTime = Calendar.getInstance()
        timeLoadingEnd= Calendar.getInstance().timeInMillis
        writeLoadingTime("Start", timeLoadingEnd-timeLoadingInit)
    }
    private fun writeLoadingTime(activity: String, time: Long) {

        val userTime = hashMapOf(
            "activity" to activity,
            "time" to time
        )
        db.collection("loadingTime").add(userTime)
    }
    override fun onStop() {
        super.onStop()
        val finalTime = Calendar.getInstance()
        val millis1: Long = initialTime.timeInMillis
        val millis2: Long = finalTime.timeInMillis

        // Calculate difference in milliseconds
        val diff = millis2 - millis1

        // Calculate difference in seconds
        val timeSpent = diff / 1000

        //save to database...

        writeNewTime("Start", timeSpent)
    }

    fun writeNewTime(activity: String, time: Long) {

        val userTime = hashMapOf(
            "activity" to activity,
            "time" to time
        )
        db.collection("userTime").add(userTime)
    }

    private fun setup(){
        val loginButton = findViewById<Button>(R.id.loginScreenButton)
        val signupButton = findViewById<Button>(R.id.signupScreenButton)

        loginButton.setOnClickListener {
            goSignIn()
        }

        signupButton.setOnClickListener {
            goSignUp()
        }

    }

    private fun goSignIn(){
        val loginIntent = Intent(this, LoginActivity::class.java)
        startActivity(loginIntent)
    }

    private fun goSignUp(){
        val signupIntent = Intent(this, SignUpActivity::class.java)
        startActivity(signupIntent)
    }

    override fun onDestroy() {
        super.onDestroy()
        val finalTime = Calendar.getInstance()
        val millis1: Long = startAppTime.timeInMillis
        val millis2: Long = finalTime.timeInMillis

        // Calculate difference in milliseconds
        val diff = millis2 - millis1

        // Calculate difference in seconds
        val timeSpent = diff / 1000

        //save to database...

        writeNewTime("App", timeSpent)
    }
}