package com.example.trapp.data
import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.Room
import com.example.trapp.data.Contact.ContactRoomDAO
import com.example.trapp.data.budget.BudgetRoomDAO
import com.example.trapp.data.city.CityDAO
import com.example.trapp.data.dailyexpense.DailyExpenseRoomDAO
import com.example.trapp.data.model.*
import com.example.trapp.data.place.PlaceRoomDAO
import com.example.trapp.data.trip.TripRoomDAO


@Database(entities = arrayOf(Budget::class, DailyExpense::class, Trip::class, Place::class, City::class, Contact::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun budgetDao(): BudgetRoomDAO
    abstract fun dailyExpenseDao(): DailyExpenseRoomDAO
    abstract fun tripDao(): TripRoomDAO
    abstract fun placeDao(): PlaceRoomDAO
    abstract fun contactDao(): ContactRoomDAO
    abstract fun cityDao(): CityDAO

    companion object {
        val dbName = "room_database"
        @Volatile
        private var instance: AppDatabase? = null
        fun getInstance(context: Context) =
            instance ?: synchronized(this) {
                instance ?: create(context).also { instance = it }
            }

        private fun create(context: Context): AppDatabase {
            return Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                dbName
            ).build()
        }
    }
}