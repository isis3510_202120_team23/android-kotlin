package com.example.trapp.ui.plan

import androidx.lifecycle.ViewModel
import com.example.trapp.data.plan.PlanMarioRepository

class PlansMarioViewModel (private val planMarioRepository: PlanMarioRepository) : ViewModel(){

    suspend fun getPlans()=planMarioRepository.getPlans()
    suspend fun getPlanById(planId: String)=planMarioRepository.getPlanById(planId)

}
