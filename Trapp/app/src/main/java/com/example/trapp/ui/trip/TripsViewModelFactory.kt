package com.example.trapp.ui.trip

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.trip.TripRepository

class TripsViewModelFactory(private val tripRepository: TripRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return TripsViewModel(tripRepository) as T
    }

}