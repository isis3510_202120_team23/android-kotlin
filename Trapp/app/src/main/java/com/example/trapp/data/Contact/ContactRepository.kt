package com.example.trapp.data.Contact

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.provider.ContactsContract
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.model.Contact
import com.example.trapp.data.model.Place
import com.example.trapp.data.place.PlaceDAO
import com.example.trapp.data.place.PlaceRepository
import com.example.trapp.data.place.PlaceRoomDAO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*


class ContactRepository private constructor( private val contactRoomDAO: ContactRoomDAO) {
    suspend fun getContacts(context: Context): List<Contact> {

        val contactsRoomLive = MutableLiveData<List<Contact>>()

        val contactsRoom = withContext(Dispatchers.IO) {contactRoomDAO.getAll()}


        if(contactsRoom.isNotEmpty())
        {
            if(Calendar.getInstance().timeInMillis - contactsRoom[0].saveDate!! < 6.048e+8)
            {
                contactsRoomLive.value = contactsRoom
                return contactsRoom
            }
            //skip
        }

      val contacts = getContactList(context)

        withContext(Dispatchers.IO){contactRoomDAO.deleteAll()}

        for(contact in contacts)
        {
            contact.saveDate = Calendar.getInstance().timeInMillis
            withContext(Dispatchers.IO){contactRoomDAO.insertAll(contact)}
        }
        contactsRoomLive.value=contacts

        return contacts

    }
    @SuppressLint("Range")
    private fun getContactList(context: Context): MutableList<Contact> {
        var contactList: MutableList<Contact> = emptyList<Contact>().toMutableList()
        val cr = context.contentResolver
        val cur = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null
        )
        if (cur?.count ?: 0 > 0) {
            while (cur != null && cur.moveToNext()) {
                val id = cur.getString(
                    cur.getColumnIndex(ContactsContract.Contacts._ID)
                )
                val name = cur.getString(
                    cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )
                if (cur.getInt(
                        cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER
                        )
                    ) > 0
                ) {
                    val pCur = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    while (pCur!!.moveToNext()) {
                        val phoneNo = pCur.getString(
                            pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER
                            )
                        )
                        Log.i(ContentValues.TAG, "Name: $name")
                        Log.i(ContentValues.TAG, "Phone Number: $phoneNo")
                        val contact = Contact(0,id, name, phoneNo)
                        contactList.add(contact)
                    }
                    pCur.close()
                }

            }
        }
        cur?.close()
        return contactList
    }
    companion object {
        @Volatile
        private var instance: ContactRepository? = null

        fun getInstance(contactRoomDAO: ContactRoomDAO) =
            instance ?: synchronized(this) {
                instance ?: ContactRepository(contactRoomDAO).also { instance = it }
            }
    }
}