package com.example.trapp.data.model

data class Activity(
    // es diferente al campo id del documento
    val firebaseId:String,
    val id: Int,
    // que corresponde a document.data?.get("id") as String y no a document.id
    val placeId: Int,
    val name:String,
    val price:Long,
    val StartDate: String,
)