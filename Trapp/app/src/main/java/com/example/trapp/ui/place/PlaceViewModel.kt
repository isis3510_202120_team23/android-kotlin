package com.example.trapp.ui.place

import androidx.lifecycle.ViewModel
import com.example.trapp.data.model.PlaceReview
import com.example.trapp.data.place.PlaceRepository

class PlaceViewModel (private val placeRepository: PlaceRepository) : ViewModel(){

    suspend fun getPlaces()=placeRepository.getPlaces()
    suspend fun getPlaceById(placeId:String)=placeRepository.getPlaceById(placeId)
    suspend fun addNewReview(placeId:String, review:PlaceReview)=placeRepository.addNewReview(placeId, review)

}
