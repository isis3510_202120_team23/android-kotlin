package com.example.trapp.ui.city

import android.content.Context
import android.util.ArrayMap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.trapp.data.city.CityRepository
import com.example.trapp.data.model.City

class

CitiesViewModel(private val cityRepository: CityRepository) : ViewModel() {

    // key: String = id de la ciudad
    // value: City = objeto City asociado al id
    private val cityList = ArrayMap<String, City>()
    private val cities = MutableLiveData<ArrayMap<String, City>>()

    init{
        cities.value=cityList
    }

    fun getCities(context: Context): LiveData<ArrayMap<String, City>> {
        cities.postValue(cityRepository.getCities(context))
        return cities as LiveData<ArrayMap<String, City>>
    }


}