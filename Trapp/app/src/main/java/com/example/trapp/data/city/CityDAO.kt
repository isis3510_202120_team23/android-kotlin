package com.example.trapp.data.city

import androidx.room.*
import com.example.trapp.data.model.City

@Dao
interface CityDAO {

    @Query("SELECT * FROM city")
    fun getAll(): List<City>

    @Query("SELECT * FROM city WHERE id = :id")
    fun getById(id: Int): City

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(city: City)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg city: City)

    @Query("DELETE FROM trip")
    fun deleteAll()

    @Delete
    fun delete(city: City)

    @Update
    fun update(city: City)

}