package com.example.trapp.ui.place.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trapp.R
import com.example.trapp.data.model.PlaceReview
import com.example.trapp.ui.budget.BudgetInput
import com.example.trapp.ui.place.PlaceViewModel
import com.example.trapp.ui.place.ReviewAdapter
import com.example.trapp.utilities.InjectorUtils
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SecondFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SecondFragment : Fragment() {

    var cm: ConnectivityManager? = null
    var activeNetwork: NetworkInfo? = null

    private var reviews: MutableList<PlaceReview> = emptyList<PlaceReview>().toMutableList()
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch(){
            initializeUI()
        }
    }


    suspend fun initializeUI()
    {
        val reviewsRecycler: RecyclerView = requireView().findViewById(R.id.reviewsRecycler)
        val reviewsAdapter = ReviewAdapter(reviews)
        val layoutReviewsManager = LinearLayoutManager(context)
        layoutReviewsManager.orientation = LinearLayoutManager.VERTICAL
        reviewsRecycler.layoutManager = layoutReviewsManager
        reviewsRecycler.adapter = reviewsAdapter

        param1?.let { loadReviews(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_second, container, false)


    }

    private fun getInternet(): Boolean {
        cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        activeNetwork = cm!!.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        return isConnected
    }

    private fun reloadReviews() {
        var reviewsRecycler: RecyclerView = requireView().findViewById(R.id.reviewsRecycler)

        reviewsRecycler.adapter?.notifyDataSetChanged()
    }

    private suspend fun loadReviews(placeId: String){

        if(!getInternet())
        {
            val reviewsRecycler: RecyclerView = requireView().findViewById(R.id.reviewsRecycler)
            val noConnection: ConstraintLayout = requireView().findViewById(R.id.constraintLayout)

            noConnection.visibility = VISIBLE
            reviewsRecycler.visibility = GONE
        }
        else {



            val factoryPlace = InjectorUtils.providePlacesViewModelFactory(requireContext())
            val viewModelPlace =
                ViewModelProviders.of(this, factoryPlace).get(PlaceViewModel::class.java)

            viewModelPlace.getPlaceById(placeId).observe(viewLifecycleOwner, { place ->
                Log.d("12321321", place.toString())
                reviews.clear()
                reviews.addAll(place.reviews as MutableList<PlaceReview>)
                Log.d("REEEEVIEWWWSSS", reviews.toString())
                reloadReviews()
            })
        }
        }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SecondFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SecondFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }

        fun newInstance() =
            SecondFragment()
    }
}