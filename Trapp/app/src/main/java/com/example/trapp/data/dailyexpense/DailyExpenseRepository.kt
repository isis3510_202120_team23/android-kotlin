package com.example.trapp.data.place

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trapp.data.budget.BudgetRoomDAO
import com.example.trapp.data.dailyexpense.DailyExpenseRoomDAO
import com.example.trapp.data.model.Budget
import com.example.trapp.data.model.DailyExpense
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class DailyExpenseRepository private constructor(private val dailyExpenseRoomDAO: DailyExpenseRoomDAO) {

    suspend fun getDailyExpensesByBudgetId(budget_id: Long): LiveData<List<DailyExpense>> {

        val dailyExpensesRoomLive = MutableLiveData<List<DailyExpense>>()

        val dailyExpensesRoom = withContext(Dispatchers.IO) {dailyExpenseRoomDAO.getByBudgetId(budget_id)}

        if(dailyExpensesRoom.isNotEmpty())
        {
            dailyExpensesRoomLive.value = dailyExpensesRoom
        }
        return dailyExpensesRoomLive;
    }

    suspend fun insertDailyExpense(dailyExpense:DailyExpense){
        withContext(Dispatchers.IO) {dailyExpenseRoomDAO.insert(dailyExpense)}
    }

    companion object {
        @Volatile
        private var instance: DailyExpenseRepository? = null

        fun getInstance(dailyExpenseRoomDAO: DailyExpenseRoomDAO) =
            instance ?: synchronized(this) {
                instance ?: DailyExpenseRepository(dailyExpenseRoomDAO).also { instance = it }
            }
    }


}