package com.example.trapp.ui.plan

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.trapp.data.plan.PlanMarioRepository
import com.example.trapp.data.plan.PlanRepository

class PlansViewModelFactory (private val planRepository: PlanRepository): ViewModelProvider.NewInstanceFactory() {


    override fun <T: ViewModel? > create(modelClass: Class<T>):T{
        return PlansViewModel(planRepository) as T
    }

}
