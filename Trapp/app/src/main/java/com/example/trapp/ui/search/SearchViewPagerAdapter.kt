package com.example.trapp.ui.search

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.trapp.ui.fragments.notfound.NotFoundFragment
import com.example.trapp.ui.fragments.packitem.PackItemFragment
import com.example.trapp.ui.fragments.place.PlaceFragment
import com.example.trapp.ui.fragments.trip.TripFragment

class SearchViewPagerAdapter(container: FragmentActivity) : FragmentStateAdapter(container){

    //TODO: Cambiar cuando se tengan el resto de fragmentos
    private val totalTabs:Int = 3

    override fun getItemCount(): Int = totalTabs

    override fun createFragment(position: Int): Fragment {
        return when (position){
            PLACESPOSITION -> PlaceFragment()
            TRIPSPOSITION -> TripFragment()
            ITEMSPOSITION -> PackItemFragment()
            else -> NotFoundFragment()
        }
    }
}