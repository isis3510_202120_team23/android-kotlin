const functions = require("firebase-functions");

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

const admin = require("firebase-admin");
admin.initializeApp();

const db = admin.firestore();

const trips = db.collection("trips");
const places = db.collection("places");
const placesTest = db.collection("places_test");


exports.search = functions.https.onCall((data, context) => {
  
  const searchterm = data.text;

  // [END readMessageData]
  // [START messageHttpsErrors]
  // Checking attribute.
  if (!(typeof searchterm === "string") || searchterm.length === 0) {
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError("invalid-argument",
        "The function must be called with " +
        "one arguments \"text\" containing the message text to add.");
  }
  // Checking that the user is authenticated.
  if (!context.auth) {
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError("failed-precondition",
        "The function must be called " +
        "while authenticated.");
  }
  // [END messageHttpsErrors]

  // [START returnMessageAsync]
  try {

        // no se que buscar con trips, sacar los destinos acá es muy costoso. 

        // let trips=[]
        
        // await trips.get();
        // snapshot.forEach(doc => {
        //   trips.push(doc.data());
        // });
        
        let placesdocs=[]
        
        await places.get();
        snapshot.forEach(doc => {
          let distance = editDistance(searchterm.toLowerCase(), doc.data().name.toLowerCase());
          places.push({ distance:distance,document:doc.data() });

        });
    
        await placesTest.get();
        snapshot.forEach(doc => {
          places.push(doc.data());
        });
    
        let result = sortBy(placesdocs, 'distance');
    
        return { result:  };
    } catch (error) {
        // throwing the error as HttpsError so that the client gets the error.
        throw new functions.https.HttpsError("unknown", error.message, error);
    }
  // [END_EXCLUDE]
});


/* calculate  the true Damerau-Levenshtein distance with adjacent transpositions */
function editDistance(string1, string2) {
  const m = string1.length;
  const n = string2.length;
  const d = new Array(m + 1);
  for (let i = 0; i <= m; i++) {
    d[i] = new Array(n + 1);
    d[i][0] = i;
  }
  for (let j = 0; j <= n; j++) {
    d[0][j] = j;
  }
  for (let j = 1; j <= n; j++) {
    for (let i = 1; i <= m; i++) {
      if (string1[i - 1] === string2[j - 1]) {
        d[i][j] = d[i - 1][j - 1];
      } else {
        d[i][j] = Math.min(
          d[i - 1][j] + 1,
          d[i][j - 1] + 1,
          d[i - 1][j - 1] + 1
        );
      }
      if (i > 1 && j > 1 && string1[i - 1] === string2[j - 2] && string1[i - 2] === string2[j - 1]) {
        d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + 1);
      }
    }
  }
  return d[m][n];
}
